

Chapter One: Analysis of algorithms - exercises and challenges
**************************************************************


Section 1. Reviewing the theory
-------------------------------

Complete the following pieces of code (they can also be found in Chapter One), and run the programs.
For each program, provide (1) an asymptotis analysis and (2) an experimental analysis of the time consumption,
using the ``time()`` function of the ``time`` module, that returns the seconds elapsed since a benchmark time;
run ``time()`` before and after the run of the algorithm, to  measure the elapsed time as the difference of the two values.
Use the Codelens for stepping through the code.

The first program ``sumofn`` is an example of how to do it; it
prints ten times the result (the sum of the first 1,000 integers) and the amount of time required for the calculation.
Check what happens if the sum of the first 10,000 integers is calculated, with respect to the results of the
asymptotic analysis (the time for each run should require 10 times more seconds).

.. activecode:: sum-of-n2
   :language: python
   :caption: Prints the sum of the first n integers

   import time

   def sumofn(n):
     start = time.time()
     sum = 0
     for i in range(1,n+1):
       sum = sum + i
     end = time.time()
     return sum,end-start

   for i in range(10):
     print("Sum is %d required %10.7f seconds"%sumofn(1000))



.. activecode:: max-list
   :language: python
   :caption: Returns the maximun element from a list

   def find_max(data):
     # Return the maximum element from a list
     biggest = data[0]
       for val in data:
         if val > biggest:
           biggest = val
       return biggest

      # INSERT NEW CODE HERE



.. tabbed:: prefix-average

   .. tab:: Quadratic time

      .. activecode:: prefix-average1
         :language: python
         :caption: Quadratic time solution of the prefix average problem

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # list of n zeros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # the j-th average
           return A

          # INSERT NEW CODE HERE


   .. tab:: Linear time

       .. activecode:: prefix-average2
         :language: python
         :caption: Linear time solution of the prefix average problem

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # list of n zeros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # the j-th average
           return A

          # INSERT NEW CODE HERE



.. tabbed:: element-uniqueness

   .. tab:: Quadratic time

      .. activecode:: element-uniqueness1
         :language: python
         :caption: Quadratic time solution of the element uniqueness problem

         def unique1(S):
           for j in range(len(S)):
             for k in range(j+1, len(S)):
               if S[j] == S[k]:
                 return False        # there is a duplicate pair
           return True               # all elements are distinct

            # INSERT NEW CODE HERE

   .. tab:: Logaritmic time

       .. activecode:: element-uniqueness2
         :language: python
         :caption: nlogn time solution of the element uniqueness problem

         def unique2(S):
           temp = sorted(S)            # sorting of S
           for j in range(1, len(temp)):
             if temp[j-1] == temp[j]:
               return False            # there is duplicate pair
           return True                 # all elements are distinct

                       # INSERT NEW CODE HERE




Section 2. The anagram detection problem
----------------------------------------

Given two strings of characters of equal length, the solution of the *anagram detection problem* consists in
writing a function that returns ``true`` if the first string is a rearrangment of the second,
``false`` otherwise.
There are several solution to this problem, with different time complexity. In what follows, we introduce
some of them; before revealing the solutions, try and figure out the algorithm.

Section 2.1 Checking off the characters
=======================================

If the strings are of the same length, and if each character in the first string occurs in the second,
then the two strings are anagrams.
Every time a character of the first list is found in the second, it has to be *checked off*, meaning that it will be
repleaced with ``None`` in the second string.
If all characters are checked off, the function will return ``True``.
This function can be revealed in the following ActiveCode; try and change the strings in order to see how the function
works.

.. reveal:: check-off
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: checking-off
       :language: python
       :caption: Detects anagrams checking-off the characters

       def anagramdetection1(s1,s2):
         stillgood = True
         if len(s1) != len(s2):
           stillgood = False
         alist = list(s2)

         pos1 = 0
         while pos1 < len(s1) and stillgood:
           pos2 = 0
           found = False
           while pos2 < len(alist) and not found:
               if s1[pos1] == alist[pos2]:
                 found = True
               else:
                 pos2 = pos2 + 1

           if found:
             alist[pos2] = None
           else:
             stillgood = False
           pos1 = pos1 + 1
       return stillgood

       print(anagramSolution1('ajcd','dcba'))


Note that for the \\(i\\)-th character in the list ``s1`` there will be \\(i\\) visits on ``s2``, for \\(i=0 \\ldots n\\).
Thus, the overall number of visits is the sum of the integers from 1 to \\(n\\), meaning that the time
comsumption is \\(O(n^2)\\).


Section 2.2 Sorting the strings
===============================

Another solution consists in sorting the two strings alphabetically,
converting each string into a list and using the built-in ``sort`` method on lists;
if they are anagrams, they will be the same string after being ordered.
The following ActiveCode shows this solution.
Again, try to figure out the algorithm before revealing the solution.
Try and change the strings to see how the function  works.

.. reveal:: sorting
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: sorting-the-strings
       :language: python
       :caption: Detects anagrams sorting the strings

       def anagramdetection2(s1,s2):
         alist1 = list(s1)
         alist2 = list(s2)

         alist1.sort()
         alist2.sort()

         pos = 0
         matches = True

         while pos < len(s1) and matches:
           if alist1[pos]==alist2[pos]:
              pos = pos + 1
           else:
              matches = False

         return matches

         print(anagramSolution2('abcde','edcba'))

The ``while`` section of the previous program takes \\(O(n)\\) time to be performed,
since there are \\(n\\) comparison on the characters after the sorting process.
But the sorting methods require \\(O(n^2)\\) or \\(O(n logn)\\) time, so the sorting operations dominate the iteration.



Section 2.3 Count and Compare
=============================

If two strings are anagrams, they will have the same number of a’s, b’s, c’s, and so on.
First, we could count the number of times each character occurs, using a list of 26 counters, 
one for each possible character.
Each time a given character is met, the counter at that position will be incremented.
If the two lists of counters are identical, the strings must be anagrams.

.. reveal:: counting
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: counting-the-characters
       :language: python
       :caption: Detects anagrams counting the characters

       def anagramSolution4(s1,s2):
         c1 = [0]*26
         c2 = [0]*26

         for i in range(len(s1)):
           pos = ord(s1[i])-ord('a')
           c1[pos] = c1[pos] + 1

         for i in range(len(s2)):
           pos = ord(s2[i])-ord('a')
           c2[pos] = c2[pos] + 1

         j = 0
         stillOK = True
         while j<26 and stillOK:
           if c1[j]==c2[j]:
              j = j + 1
           else:
              stillOK = False

         return stillOK

       print(anagramSolution4('ajcd','dcba'))


The first two iterations, used to count the characters, are based on the length of each string;
the third iteration takes 26 steps. The overall time consumption is \\(O(n)\\), that is a linear-time
algorithm. Note that even if this is an optimal solution, this result is achieved by means of two small
additional lists that keep the characters' counts. This means that we have to accept a trade-off
between time and space usage.


Section 2.4 Finding all possible anagrams
=========================================

A *brute force* technique for solving this problem tries
generate a list of all possible strings using the characters from ``s1`` and then check if ``s2`` occurs among them.
The total number of candidate strings is \\(n∗(n−1)∗(n−2)∗ \\ldots ∗3∗2∗1\\), which is \\(n!\\).
The factorial function grows faster than \\(2^n\\), meaning that a brute force approach is not advisable.



Section 3. Exercises and self-evaluation
----------------------------------------

Give a \\(O\\) characterization, in terms of \\(n\\), of the running time of the following code fragment
(Hints: consider the number of times the loop is executed and how many primitive operations occur in each iteration)

**Exercise 3.1**::

 1  def example1(S):
 2  ”””Return the sum of the elements in sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):     # loop from 0 to n-1
 6      total += S[j]
 7    return total

**Exercise 3.2**::

 1  def example2(S):
 2  ”””Return the sum of the elements with even index in sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(0, n, 2):     # increment of 2
 6      total += S[j]
 7    return total

**Exercise 3.3**::

 1  def example3(S):
 2  ”””Return the sum of the prefix sums of sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):         # loop from 0 to n-1
 6      for k in range(1+j):     # loop from 0 to j
 7        total += S[k]
 8    return total

**Exercise 3.4**::

 1  def example4(S):
 2  ”””Return the sum of the prefix sums of sequence S.”””
 3    n = len(S)
 4    prefix = 0
 5    total = 0
 6    for j in range(n):
 7      prefix += S[j]
 8      total += prefix
 9    return total

**Exercise 3.5**::

 1  def example5(A, B): # assume that A and B have equal length
 2  ”””Return the number of elements in B equal to the sum of prefix sums in A.”””
 3    n = len(A)
 4    count = 0
 5    for i in range(n):         # loop from 0 to n-1
 6      total = 0
 7      for j in range(n):       # loop from 0 to n-1
 8        for k in range(1+j):   # loop from 0 to j
 9          total += A[k]
 10     if B[i] == total:
 11       count += 1
 12   return count


**Exercise 3.5**

Algorithm \\(A\\) executes an \\(O(\\log n)\\)-time computation for each entry of an
\\(n\\)-element sequence. What is its worst-case running time?
(Hint: the \\(O(\\log n)\\) calculation is performed \\(n\\) times)

**Exercise 3.6**

Given an \\(n\\)-element sequence \\(S\\), algorithm \\(B\\) chooses \\(\\log n\\) elements in
\\(S\\) at random and executes an \\(O(n)\\)-time calculation for each of them. What is the
worst-case running time of \\(B\\)?
(Hint: the \\(O(n)\\) calculation is performed \\(\\log n\\) times)

**Exercise 3.7**

Given an \\(n\\)-element sequence \\(S\\) of integers, algorithm \\(C\\) executes an
\\(O(n)\\)-time computation for each even number in \\(S\\), and an \\(O(\\log n)\\)-time
computation for each odd number in \\(S\\). What are the best-case and worst-case
running times of \\(C\\)?
(Hint: consider the cases when all entries of \\(S\\) are even or odd)

**Exercise 3.8**

Given an \\(n\\)-element sequence \\(S\\), algorithm \\(D\\) calls algorithm \\(E\\) on each
element \\(S[i]\\). Algorithm \\(E\\) runs in \\(O(i)\\)-time when it is called on \\(S[i]\\).
What is the worst-case running time of \\(D\\)?
(Hint: characterize the running time of \\(D\\) using a summation)

**Exercise 3.9**

Describe an efficient algorithm for finding the ten largest elements in a
sequence of size \\(n\\). What is the running time of your algorithm?
(Hint: 10 is a constant number)

**Exercise 3.10**

Describe an algorithm for finding both the minimum and maximum of \\(n\\)
numbers using fewer than \\(3n/2\\) comparisons.
(Hint: construct a group of candidate for minimums and a group of candidate for maximums)

**Exercise 3.11**

A sequence \\(S\\) contains \\(n−1\\) unique integers in the range \\([0,n−1]\\), that
is, there is one number from this range that is not in \\(S\\). Design an \\(O(n)\\)-time
algorithm that finds that number. You can use \\(O(1)\\) additional space only.
(Hint: use a function of the integers in \\(S\\) that immediately identifies which number is missing)

**Exercise 3.12**

A sequence \\(S\\) contains \\(n\\) integers taken from the interval \\([0,4n]\\), with repetitions
allowed. Describe an efficient algorithm for determining an integer
value \\(k\\) that occurs the most often in \\(S\\). What is the running time of the algorithm?
(Hint: use an array that keeps counts for each value)
