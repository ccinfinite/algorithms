

Chapter Five: Linked lists - exercises and challenges
*****************************************************

Section 1. Reviewing the theory
-------------------------------

**Exercise 1.1**

Complete and execute the following piece of code (that can be found in Chapter Five),
in which a stack ADT is implemented by means of a singly linked list.
Remember that each stack instance maintains two variables: ``_head`` (a reference to the node at the head of the list)
and ``_size`` (that keeps track of the current number of elements).
Add some examples of how the stack works, using some calls to the methods ``pop``, ``push``, or ``top``.

.. activecode:: stack-as-a-singly-linked-list
   :language: python
   :caption: Implementation of a stack by a singly linked list

   class LinkedStack:
   # Stack implementation with a singly linked list

     # Node class nested in stack
     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     # Stack methods
     def __init__(self):
       self._head = None
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def push(self, e):
       self._head = self._Node(e, self._head)
       self._size += 1

     def top(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       return self._head._element

     def pop(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       return answer

           # INSERT NEW CODE HERE


**Exercise 1.2**

Complete and execute the following piece of code (that can be found in Chapter Five),
in which a queue ADT is implemented by means of a singly linked list.
Remember that we perform operations on both ends of the queue, meaning that we have to maintain a ``_head``
and a ``_tail`` references as instance variables for each queue, together with its ``_size``.
The front of the queue is aligned with the head of the list, and the back of the queue is aligned with the tail of the list,
allowing us to enqueue elements at the back, and dequeue them from the front.
Add some examples of how the queue works, using some calls to the methods ``first``, ``enqueue``, or ``dequeue``.

.. activecode:: queue-as-a-singly-linked-list
   :language: python
   :caption: Implementation of a queue by a singly linked list

   class LinkedQueue:
   # Queue implementation with a singly linked list

     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__ (self):
       self._head = None
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       if self.is_empty( ):       # special case if queue is empty
         self._tail = None        # removed head had been the tail
       return answer

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         self._head = newnode
       else:
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

              # INSERT NEW CODE HERE



**Exercise 1.3**

Complete and execute the following piece of code (that can be found in Chapter Five),
in which a queue ADT is implemented by means of a circularly linked list.
Remember that the queue has a head and a tail, with the next reference of the tail linked to the head.
We have to mantain the instance variables ``_tail`` (a reference to the tail node),
and ``_size`` (the number of elements in the queue); the head of the queue is found by following the tail’s next reference,
that is ``self._tail._next``.
With the method ``rotate``, the old head becomes the new tail,
and the node after the old head becomes the new head.
Add some examples of how the queue works, using some calls to the methods ``first``, ``enqueue``, or ``dequeue``.


.. activecode:: queue-as-a-circularly-list
   :language: python
   :caption: Implementation of a queue by means of a circularly list

   class CircularQueue:
   # Queue implementation using circularly linked list

     class _Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__(self):
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       head = self._tail._next
       return head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       oldhead = self._tail._next
       if self._size == 1:
         self._tail = None
       else:
         self._tail._next = oldhead._next
       self._size −= 1
       return oldhead._element

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         newnode._next = newnode
       else:
         newnode._next = self._tail._next
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

     def rotate(self):
       if self._size > 0:
         self._tail = self._tail._next


      # INSERT NEW CODE HERE


**Exercise 1.4**

Complete and execute the following piece of code (that can be found in Chapter Five),
in which a deque ADT is implemented by means of a doubly linked list.
Note that the class ``LinkedDeque`` inherits from the class ``_DoublyLinkedBase``,
using its methods to initialize a new instance to insert an element at the front or at the end,
and to delete an element.
The first element of the deque is stored after the header,
and the last element of the deque is stored in the node before the trailer.
Add some examples of how the deque works, using some calls to the methods ``insert_first``, ``insert_last``,
``delete_first``, and ``delete_last``.

.. activecode:: deque-as-a-doubly_linked-list
   :language: python
   :caption: Implementation of a deque by means of a doubly linked list

   class _DoublyLinkedBase:

     class Node:
       __slots__ = '_element' , '_prev' , '_next'
       def __init__(self, element, prev, next):
         self._element = element
         self._prev = prev                # previous node reference
         self._next = next                # next node reference

     def __init__(self):
       self._header = self._Node(None, None, None)
       self._trailer = self._Node(None, None, None)
       self._header._next = self._trailer        # trailer is after header
       self._trailer._prev = self._header        # header is before trailer
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def _insert_between(self, e, predecessor, successor):
       newnode = self._Node(e, predecessor, successor)
       predecessor._next = newnode
       successor._prev = newnode
       self._size += 1
       return newnode

     def _delete_node(self, node):
       predecessor = node._prev
       successor = node._next
       predecessor._next = successor
       successor._prev = predecessor
       self._size −= 1
       element = node._element                             # record deleted element
       node._prev = node._next = node._element = None      # useful for the garbage collection
       return element                                      # return deleted element

   class LinkedDeque(_DoublyLinkedBase):

     def first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._header._next._element

     def last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._trailer._prev._element

     def insert_first(self, e):
       self._insert_between(e, self._header, self._header._next)

     def insert_last(self, e):
       self._insert_between(e, self._trailer._prev, self._trailer)

     def delete_first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._header._next)

     def delete_last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._trailer._prev)




Section 2. Problems on linked lists
-----------------------------------

In this section, we will cope with some classical problems that can be solved using linked lists.
Study each exercise, then check, complete (if needed) and run the ActiveCode provided.

**Exercise 2.1**

Write a function to count the number of nodes in a given singly linked list, in both iterative and recursive version.
The iterative solution to this problem is in the following pseudocode::

 initialize count as 0
 initialize a node pointer, current = head.
 do following while current is not NULL
   current = current -> next
   count++;
 return count

Check, complete and run the following ActiveCode.

.. activecode:: number-of-nodes-list-iterative
   :language: python
   :caption: Returns the number of nodes in a list, with an iteration

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCount(self):
       temp = self.head
       count = 0
       while (temp):
         count += 1
         temp = temp.next
       return count

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print ("Count of nodes is :",llist.getCount())


The recursive solution to this problem is in the following pseudocode::

 int getCount(head)
   if head is NULL, return 0
   else return 1 + getCount(head->next)

Check, complete and run the following ActiveCode.

.. activecode:: number-of-nodes-list-iterative-2
   :language: python
   :caption: Returns the number of nodes in a list, with an iteration

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCountRec(self, node):
        if (not node):
          return 0
        else:
          return 1 + self.getCountRec(node.next)

     def getCount(self):
       return self.getCountRec(self.head)

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print 'Count of nodes is :',llist.getCount()


**Exercise 2.2**

Given a Linked List and a number \\(n\\), write a function that returns the value at the
\\(n\\)-th node from the end of the Linked List.
The straightforward solution consists in calculating the length *len* of the list,
and then print the *len-n+1*-th node from the beginning of the list.
The ActiveCode is given below.

.. activecode:: Nth-node-from-the-end
   :language: python
   :caption: Returns the n-th node from the and of the list

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       temp = self.head
       length = 0
       while temp is not None:
         temp = temp.next
         length += 1

        if n > length:
          print('Location is greater than the length of the list')
          return
        temp = self.head
        for i in range(0, length - n):
          temp = temp.next
        print(temp.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)

Another interesting solution is to maintain two pointers, the reference and the main pointers.
Initially they both point to the head of the list, and the the reference pointer is moved to \\(n\\) nodes from head.
Then, both pointers are moved one by one until the reference pointer reaches the end.
Now the main pointer will point to \\(n\\)-th node from the end.
The ActiveCode is given below.

.. activecode:: Nth-node-from-the-end-2
   :language: python
   :caption: Returns the n-th node from the and of the list

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       main_ptr = self.head
       ref_ptr = self.head

       count = 0
       if(self.head is not None):
         while(count < n ):
           if(ref_ptr is None ):
             print('Location is greater than the length of the list')
             return
           ref_ptr = ref_ptr.next
           count += 1

       if(ref_ptr is None):
         self.head = self.head.next
         if(self.head is not None):
           print('The node no. %d from last is %d', n, main_ptr.data)
      else:
        while(ref_ptr is not None):
          main_ptr = main_ptr.next
          ref_ptr = ref_ptr.next
        print('The node no. %d from last is %d', n, main_ptr.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)


**Exercise 2.3**

This problem is named after Flavius Josephus, a Jewish historian who fought against the Romans.
He and his group of soldiers were cornered by the Romans inside a cave,
and they chose to murder and suicide instead of surrender and capture.
They decided that all the soldiers will sit in a circle and, starting from the soldier sitting at the first position,
every soldier will kill the soldier next to them.
So, suppose there are 5 soldiers sitting in a circle with positions numbered as 1, 2, 3, 4, 5.
The soldier 1 kills 2, then 3 kills 4, then 5 kills 1, then 3 kills 5, and since 3 is the only one left then 3 commits suicide.
Now Josephus doesn’t want to get murdered or commit suicide, and he has to figure out at which position should he sit in a circle
so that he is the last man standin, and instead of committing suicide he will surrender to the Romans.

There are several solution to this problem (in general, for \\(n=2^a +k\\) soldiers, the survivor is the \\(2k+1\\)-th soldier);
we show here a solution that uses a circular linked list.

.. activecode:: josephus-problem
   :language: python
   :caption: Josephus problem with a circular linked list

   class Node:
     def __init__(self, x):
       self.data = x
       self.next = None

   def getJosephusPosition(m, n):
     # Create a circular linked list of size n.
    head = Node(1)
    prev = head
    for i in range(2, n + 1):
      prev.next = Node(i)
      prev = prev.next
    prev.next = head

     # while only one node is left in the linked list
     ptr1 = head
     ptr2 = head
     while (ptr1.next != ptr1):
       count = 1
       while (count != m):
         ptr2 = ptr1
         ptr1 = ptr1.next
         count += 1
       ptr2.next = ptr1.next
       ptr1 = ptr2.next

     print("Last person left standing (Josephus Position) is ", ptr1.data)

   if __name__ == '__main__':
     n = 14
     m = 2
     getJosephusPosition(m, n)


**Exercise 2.4**

The task here is to create a doubly linked list, with head and tail pointers, by inserting nodes such that list
remains in ascending order on printing from left to right.
The pseudo code is as follows::

 if the list is empty, then
   make left and right pointers point to the node to be inserted
   make its previous and next field point to NULL
 if node to be inserted has value lower than the value of first node of the list, then
   connect that node from previous field of first node
 if node to be inserted has value higher than the value of last node of the list, then
   connect that node from next field of last node
  if node to be inserted has value in between the value of first and last node, then
    check for appropriate position and make connections

Check, complete and run the following ActiveCode.

.. activecode:: insert-in-sorted-list
   :language: python
   :caption: Insert a new value in a sorted list

   class Node:
   def __init__(self, data):
     self.info = data
     self.next = None
     self.prev = None

   head = None
   tail = None

   def nodeInsert( key) :
     global head
     global tail

     p = Node(0)
     p.info = key
     p.next = None

     # if first node to be inserted in doubly linked list
     if ((head) == None):
       (head) = p
       (tail) = p
       (head).prev = None
       return

     # if node to be inserted has value lower than first node
     if ((p.info) < ((head).info)):
       p.prev = None
       (head).prev = p
       p.next = (head)
       (head) = p
       return

     # if node to be inserted has value higher than last node
     if ((p.info) > ((tail).info)):
       p.prev = (tail)
       (tail).next = p
       (tail) = p
       return

     # find the node before which we need to insert p.
     temp = (head).next
     while ((temp.info) < (p.info)):
       temp = temp.next

     # insert new node before temp
     (temp.prev).next = p
     p.prev = temp.prev
     temp.prev = p
     p.next = temp

   # print nodes from left to right
   def printList(temp) :
     while (temp != None):
       print( temp.info, end = " ")
       temp = temp.next

   nodeInsert( 30)
   nodeInsert( 50)
   nodeInsert( 90)
   nodeInsert( 10)
   nodeInsert( 40)
   nodeInsert( 110)
   nodeInsert( 60)
   nodeInsert( 95)

   print("Doubly linked list on printing from left to right\n" )
   printList(head)









Section 3. Exercises and self-evaluation
----------------------------------------

**Exercise 3.1**

Write an algorithm for finding the second-to-last node in a singly linked
list in which the last node is indicated by a next reference of None.

**Exercise 3.2**

Write an algorithm for concatenating two singly linked lists L and
M, given only references to the first node of each list, into a single list L
that contains all the nodes of L followed by all the nodes of M.
(Hint: The concatenation doesn't search all elements of the two lists)

**Exercise 3.3**

Write a recursive algorithm that counts the number of nodes in a singly linked list.
(Hint: use the ``next`` pointer as a parameter for the call of the function)

**Exercise 3.4**

Write a function that counts the number of nodes in a circularly linked list.
(Hint: Keep track of the starting point, and stop when you reach it)

**Exercise 3.5**

Suppose that x and y are references to nodes of circularly linked lists,
although not necessarily the same list. Write an algorithm for telling if x and y belong to the same list.
(Hint: Go around one of the lists once)

**Exercise 3.6**

In Chapter Five, Section 2.1, an implementation of a queue with a circularly linked list can be found.
The ``CircularQueue`` class provides a ``rotate()`` method, that has semantics equivalent to Q.enqueue(Q.dequeue()); 
basically, the old head becomes the new tail, and the node after the old head becomes the new head.
Write the  same method for the ``LinkedQueue`` class of Section 1.2, where a queue is implemented with a
singly listed list.
(Hint: Adjust the links so that the first node is moved to the end of the list)

**Exercise 3.7**

Write a method for finding the middle node of a doubly linked list with header and trailer sentinels,
by means of link hopping (without using a counter).
In the case of an even number of nodes, report the node slightly left of center as the middle one.
(Hint: Search the list starting from both ends)

**Exercise 3.8**

Write a fast algorithm for concatenating two doubly linked lists L and M,
with header and trailer sentinel nodes, into a single list L.
(Hint: Appropriately connect the end of L and the beginning of M)

**Exercise 3.9**

Write a method ``max(L)`` that returns the maximum element from a ``PositionalList`` instance L
containing integers.

**Exercise 3.10**

Update the ``PositionalList`` class of Chapter Five, Section 4.2, to support an additional method ``find(e)``,
which returns the position of the (first occurrence of) element e in the list.

**Exercise 3.11**

Describe an implementation of the ``PositionalList`` methods ``add_last`` and
``add_before``, using only the methods ``is_empty``, ``first``, ``last``,
``prev``, ``next``, ``add_after``, and ``add_first``.

**Exercise 3.12**

Write a complete implementation of the stack ADT and of the queue ADT by means of a singly linked 
list that uses  a header sentinel.

**Exercise 3.13**

Write a method ``concatenate(Q2)`` for the ``LinkedQueue`` class that
takes all elements of ``Q2`` and appends them to the end of the original queue.
(Hint: Work on head and tail members of both lists)

**Exercise 3.14**

Give a recursive implementation of a singly linked list class, such that an instance of a nonempty list
stores its first element and a reference to a list of remaining elements.
Then, write a recursive algorithm for reversing the list.
(Hint: The chain of nodes following the head node are a list themselves;
to reverse the list, recur on the first \\(n−1\\) positions)

**Exercise 3.15**

Implement a ``bubble-sort`` function that takes a list L as a parameter (singly or doubly linked).
(Hint: bubble-sort scans the list \\(n−1\\) times; for each scan, it compares the current element
with the next one, and swaps them if they are out of order.
A ``swap`` function would be of help)

**Exercise 3.16**

An array \\(A\\) is *sparse* if most of its entries are empty.
A list L can be used to implement such an array efficiently.
For each nonempty cell \\(A[i]\\), we can store an entry \\((i,e)\\) in L, where e is the
element stored at \\(A[i]\\).
This allows us to represent \\(A\\) using \\(O(m)\\) storage, with \\(m\\) the number of nonempty entries in \\(A\\).
Write the ``SparseArray`` class with the methods ``getitem(j)`` and ``setitem (j,e)``.
