
Chapter Two: Recursion - exercises and challenges
*************************************************


Section 1. Reviewing the theory
-------------------------------

Complete and execute the pieces of code in the following sections (the code can also be found in Chapter Two).

**Exercise 1.1**

The first solution for finding a target value into an unsorted sequence the *sequential search*.
A loop is used to examine every element, until either the target has been found or the data set is exhausted.
*Binary search* is an algorithm that finds a target value inside a sorted indexable sequence of \\(n\\) elements,
using recursion in a more efficient way. Use the function defined in the following ActiveCode to find
a target value in an array of integers that has been previously sorted.

.. activecode:: binary-search
   :language: python
   :caption: Function for the binary search

   def binary_search(data, target, low, high):
     # Return True if target is found in data, between low and high
     if low > high:
       return False               # no match found
     else:
       mid = (low + high) / 2
       if target == data[mid]:    # found a match
         return True
       elif target < data[mid]:
         return binary_search(data, target, low, mid - 1)   # call on the left portion
       else:
         return binary_search(data, target, mid + 1, high)  # call on the right portion

        # INSERT NEW CODE HERE


**Exercise 1.2**

Consider the the problem of reversing the elements of \\(A\\), and indexed sequence;
the first element becomes the last, the second element becomes second to the last, and so on.
A recursive implementation is given below. Use it to reverse an array.

.. activecode:: reverse-a-sequence
   :language: python
   :caption: Function for reversing a sequence of elements

   def reverse(A, first, last):
     # Reverse elements in A, between first and last
     if first < last - 1:                           # if there are at least 2 elements
       A[first], A[last-1] = A[last-1], A[first]    # swap first and last
       reverse(A, first+1, last-1)

        # INSERT NEW CODE HERE


**Exercise 1.3**

Consider the problem of summing the elements of an indexed sequence \\(A\\).
The sum of all \\(n\\) integers in \\(A\\) is the sum of the first \\(n-1\\) integers in \\(A\\),
plus its last element. We provide the Python coding below; use it on a sequence of elements.

.. activecode:: linear-sum
   :language: python
   :caption: Linear sum of a sequence of elements

   def linear_sum(A, n):
     # Returns the sum of the first n numbers of A
     if n == 0:
       return 0
     else:
       return A[n-1] + linear_sum(A, n-1)

    # INSERT NEW CODE HERE


Instead of summing the last element of \\(A\\) with the sum of the remaining elements,
we compute the sum of the first half of \\(A\\) and the sum of the second half of \\(A\\),
recursively, and then add these numbers.

.. activecode:: binary-sum
   :language: python
   :caption: Binary sum of a sequence of elements

   def binary_sum(A, first, last):
     # Return the sum of the numbers in A between first and last
     if first <= last:     # no elements
       return 0
     elif first == last-1:     # one element
       return A[first]
     else:
       mid = (first + last) / 2
       return binary_sum(A, first, mid) + binary_sum(A, mid, last)

    # INSERT NEW CODE HERE


**Exercise 1.4**

The Fibonacci numbers can be defined as \\(F_0 =0\\), or \\(F_1 = 1\\), or \\(F_n = F_{n-2} + F_{n-1}\\).
The Python code for this function is in the following ActiveCode; try and see if you can run this function.

.. activecode:: bad-fibonacci
   :language: python
   :caption: Hard to compute Fibonacci function

   def fibonacci(n):
     if n <= 1:
       return n
     else:
       return fibonacci(n-2) + fibonacci(n-1)

    # INSERT NEW CODE HERE


A more efficient way to define this program is as follows; compare it to the previous code.

.. activecode:: good-fibonacci
   :language: python
   :caption: Easy to compute Fibonacci function

   def good_fibonacci(n):
     if n <= 1:
       return (n,0)
     else:
       (a,b) = good_fibonacci(n-1)
       return (a+b,a)

    # INSERT NEW CODE HERE


**Exercise 1.5**

The *element uniqueness problem* consists in deciding whether all elements of a
sequence \\(A\\) are distinct from each other.

The first solution is an iterative algorithm. Use this function on a generic array.

.. activecode:: iterative-uniqueness
   :language: python
   :caption: Iterative solution to the element uniqueness problem

   def unique1(A):
     for i in range(len(A)):
       for j in range(i+1, len(A)):
         if A[i] == A[j]:
           return False
     return True

    # INSERT NEW CODE HERE


An inefficient recursive implementation is given below.

.. activecode:: recursive-uniqueness
   :language: python
   :caption: Recursive solution to the element uniqueness problem

   def unique3(A, first, last):
     # Returns True if there are no duplicate elements in A[first:last]
     if last - first <= 1:
       return True
     elif not unique3(A, first, last-1):
       return False
     elif not unique3(a, first+1, last):
       return False
     else:
       return A[first] != A[last-1]

     # INSERT NEW CODE HERE


An improved solution is given in the following script.

.. activecode:: improved-recursive-uniqueness
   :language: python
   :caption: Improved recursive solution to the element uniqueness problem

   def unique(A, first, last):
     if last-first <= 1:
       return True
     elif A[first] in A[first+1:]:
       return False
     else:
       return unique(A, first+1, last)

     # INSERT NEW CODE HERE




Section 2. Problems on recursion
--------------------------------

In this section, we will show some classical problem that can be solved by means of
recursive functions; check, complete and run the ActiveCode in each subsection.

**Exercise 2.1**

The *Tower of Hanoi* puzzle was invented by the French mathematician Edouard Lucas, in 1883.
A legend tells of a temple where the priests were given three poles and a stack of 64 gold disks,
each disk smaller than the one beneath it.
They had to transfer all 64 disks from one of the three poles to another, with two rules:
first, only one disk at a time could be moved; then, a larger disk couldn't be placed on top of a smaller one.
When they finished their work, the legend said, the world would vanish.

The number of moves required to correctly move a tower of 64 disks is \\(2^{64} -1\\), that is 18.446.744.073.709.551.615.
Moving one disk per second, the priest will need 584.942.417.355 years.

In order to find a recursive solution to this problem, consider the following example.
We have three pegs, namely peg one, peg two, and peg three, and suppose we have a tower of four disks, originally on peg one.
If we already know how to move a tower of three disks to peg two, we can easily move the bottom disk to peg three,
and then move the tower of three from peg two to peg three.
We do not know how to move a tower of height three, but if we know how to move a tower of height two,
we can move the third disk from one peg to the other, and move the tower of two from one peg on top of it.
At the end of this (recursive) procedure, we will be able to move a single peg.

The high-level outline of how to move a tower from the starting pole to the goal pole using an intermediate pole is::

 Move a tower of height-1 to an intermediate pole, using the final pole
 Move the remaining disk to the final pole
 Move the tower of height-1 from the intermediate pole to the final pole using the original pole

As long as we always obey the rule that the larger disks remain on the bottom of the stack,
we can use the three steps above, recursively. We hit the base case of the recursion when we have a tower of one disk;
in this case, we need to move only a single disk to its final destination.
The Python function is defined as follows (note the order of ``fromPole``, ``toPole``, and ``withPole`` in the
recursive calls of ``moveTower``)::

 def moveTower(height, fromPole, toPole, withPole):
    if height >= 1:
        moveTower(height-1, fromPole, withPole, toPole)
        moveDisk(fromPole, toPole)
        moveTower(height-1, withPole, toPole, fromPole)

The previous code makes two different recursive calls: with the first one,
all but the bottom disk are moved from the initial tower to an intermediate pole;
the next line moves the bottom disk to its final resting place;
the second recursive call moves the tower from the intermediate pole to the top of the largest disk.
The base case is detected when the tower height is 0, and in this case there is nothing to do.

The function ``moveDisk`` just prints that it is moving a disk from one pole to another::

 def moveDisk(from,to):
   print("moving disk from", from, "to", to)

The following ActiveCode provides the entire solution.

.. activecode:: tower-of-hanoi
   :language: python
   :caption: Recursive solution for the Tower of Hanoi problem

   def moveTower(height,fromPole, toPole, withPole):
       if height >= 1:
           moveTower(height-1,fromPole,withPole,toPole)
           moveDisk(fromPole,toPole)
           moveTower(height-1,withPole,toPole,fromPole)

   def moveDisk(fp,tp):
       print("moving disk from",fp,"to",tp)

   moveTower(3,"A","B","C")



**Exercise 2.2**

The sieve of Eratosthenes is a simple algorithm for finding all prime numbers up to a specified integer \\(n\\);
a recursive function that implements this algorithm has the following pseudocode::

 a) create a list of integers 2, 3, 4, ..., n;
 b) set counter i to 2 (the first prime number);
 c) starting from i+i, count up by i and remove those numbers from the list (2*i, 3*i, ...);
 d) find the first number of the list following i; this is the next prime number;
 e) set counter i to this number;
 f) repeat steps c and d until i is greater that n.

The following program implements the sieve of Eratosthenes in an iterative way.
It will print out the first 100 prime numbers.

.. activecode:: sieve-of-eratosthenes-iterative
   :language: python
   :caption: Iterative solution for the sieve of Eratosthenes

   from math import sqrt
   def sieve(n):
     primes = list(range(2,n+1))
     max = sqrt(n)
     num = 2
     while num < max:
       i = num
       while i <= n:
         i += num
         if i in primes:
           primes.remove(i)
       for j in primes:
         if j > num:
           num = j
           break
     return primes
   print(sieve(100))

In what follows, we give a recursive solution for the same problem.

.. activecode:: sieve-of-eratosthenes-recursive
   :language: python
   :caption: Recursive solution for the sieve of Eratosthenes

   from math import sqrt
   def primes(n):
     if n == 0:
       return []
     elif n == 1:
       return []
     else:
       p = primes(int(sqrt(n)))
       nop = [j for i in p for j in range(i*2, n + 1, i)]
       p = [x for x in range(2, n + 1) if x not in nop]
       return p
     print(primes(100))



Section 3. Exercises and self-evaluation
----------------------------------------

**Exercise 3.1**

Write a recursive code of the function \\(f(n)=3*n\\), i.e. the multiples of 3.
(Hint: A recursive definition of this function can be written as \\(f(1) = 3\\) and \\( f(n+1)=f(n)+3\\) )

**Exercise 3.2**

Write a recursive code of the function \\(power(x,n)=x^n\\), i.e. the power function.
(Hint: A simple recursive definition follows from the fact that \\(x^n = x * x^{n-1}\\), for \\(n > 0\\) )

**Exercise 3.3**

Write a recursive code of the function that returns the sum of the first \\(n\\) integers.
(Hint: The solution comes from the observation that the sum of the first \\(n\\)
numbers is equal to the sum of \\(n\\) and the sum of the first \\(n-1\\) integers)

**Exercise 3.4**

Write a recursive function ``find_index()``, which returns the index of a number in the
Fibonacci sequence, if the number is an element of this sequence, and returns -1, otherwise.

**Exercise 3.5**

Describe a recursive algorithm for finding the maximum and minimum elements in a sequence of \\(n\\) integers,
without using any loops. What is your running time and space usage?
(Hint: Consider returning a tuple, which contains both the minimum and maximum value)

**Exercise 3.6**

Write a recursive function to reverse a list.

**Exercise 3.7**

Write a recursive function for the element uniqueness problem, which runs in time \\(O(n^2)\\), without using sorting.
(Hint: Telling if the elements of a sequence are unique can be reduced to the problem of telling
if the last \\(n−1\\) elements are all unique and different than the first element)

**Exercise 3.8**

Write a recursive algorithm to compute the product of two positive integers, \\(m\\) and \\(n\\), using only addition.
(Hint: The product of \\(m\\) and \\(n\\) is the sum of \\(m\\), \\(n\\) times)

**Exercise 3.9**

Write a recursive algorithm to compute the exponent of two positive integers, \\(m^n \\), using only the product.
(Hint: Figure out how the exponentiation can be defined by means of the product)

**Exercise 3.10**

Write a recursive function that takes a character string *s* and outputs its reverse.
For example, the reverse of *pots* would be *stop*.
(Hint: Print one character at a time, without extra spaces)

**Exercise 3.11**

Write a  recursive function to see if a string *s* is a palindrome, that is, it is equal to its reverse.
(Hint: Check the equality of the first and last characters and recur)

**Exercise 3.12**.

Use recursion to write a function for determining if a string *s* has more vowels than consonants.
