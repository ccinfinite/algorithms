

Chapter Four: Stack, queues, and dequeues - exercises and challenges
********************************************************************



Section 1. Reviewing the theory
-------------------------------

**Exercise 1.1**

Complete and execute the following piece of code (that can be found in Chapter Four),
in which a stack ADT is implemented by means of a Python list.
Show some examples of how the stack works, then add the the exception class ``Empty``,
that is raised when the user calls ``pop`` or ``top`` on an empty stack.

.. activecode:: stack-as-a-list
   :language: python
   :caption: Implementation of a stack by means of a list

   class ArrayStack:
   # LIFO Stack implementation using a Python list as storage

     # Creates an empty stack
     def __ init __ (self):
       self._data = [ ]

     # Returns the number of elements in the stack
     def __ len __ (self):
       return len(self._data)

     # Returns True if the stack is empty
     def is_empty(self):
       return len(self._data) == 0

     # Adds element e to the top of the stack
     def push(self, e):
       self._data.append(e)

     # Returns (but don't remove) the element at the top of the stack
     # raising an exception if the stack is empty
     def top(self):
       if self.is_empty( ):
         raise Empty( Stack is empty )
       return self._data[−1]

     # Remove and return the element from the top of the stack
     # raising an exception if the stack is empty
     def pop(self):
       if self.is_empty( ):
         raise Empty( 'Stack is empty' )
       return self._data.pop( )

           # INSERT NEW CODE HERE


**Exercise 1.2**

Write a straightforward implementation of a queue ADT, adapting a Python list;
show some examples of how the queue works (see Section 2.1 of Chapter Four for some suggestions:
when a ``pop(0)`` is called in order to remove the first element from the list,
all remaining elements must be shifted to the left).

.. activecode:: queue-as-a-list
   :language: python
   :caption: Implementation of a queue by means of a list

     # INSERT NEW CODE HERE



Then, write a more efficient implementation of a queue ADT based on a circular array (as in Section 2.2
of Chapter Four); show some examples of how the queue works.

.. activecode:: queue-as-a-circular-array
   :language: python
   :caption: Implementation of a queue by means of a circular array

   class ArrayQueue:
   # queue implemented with a list
     DEFAULT = 10          # capacity of all queues

     def __init__(self):
       self._data = [None]*ArrayQueue.DEFAULT
       self._size = 0
       self._front = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._data[self._front]

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       element = self._data[self._front]
       self._data[self._front] = None
       self._front = (self._front + 1) % len(self._data)
       self._size −= 1
       return element

     def enqueue(self, e):
       if self._size == len(self._data):
         self._resize(2*len(self.data))           # double the array size
      position = (self._front + self._size) % len(self._data)
      self._data[position] = e
      self._size += 1

     def _resize(self, capacity):
       old = self._data                   # keeps track of existing list
       self._data = [None]*capacity       # allocate list with new capacity
       walk = self._front
       for k in range(self._size):
         self._data[k] = old[walk]        # shifts the indices
         walk = (1 + walk) % len(old)     # use old size
       self._front = 0                    # realigs front

          # INSERT NEW CODE HERE


**Exercise 1.3**

Write a straightforward implementation of a double ended queue ADT, adapting a Python list;
show some examples of how the queue works (see Section 3 of Chapter Four).

.. activecode:: deque-as-a-list
   :language: python
   :caption: Implementation of a deque by means of a list

     # INSERT NEW CODE HERE




Section 2. Problems on stack, queues, deque's
---------------------------------------------

In this section, we will cope with some classical problems that can be solved using stack, queue, and deque ADT.
Study each exercise, then check, complete (if needed) and run the ActiveCode provided.

**Exercise 2.1**

Provide an algorithm that converts integer decimal numbers (i.e., numbers represented in base 10) into binary numbers
(i.e., numbers represented in base 2).

The solution to this problem will use a stack to keep track of the digits for the partial binary result.
We start with a decimal number greater than 0.
We then iterate the division of the number by 2, and we push the remainder (0 or 1) in the stack.
This means that we are building the binary number as a sequence of digits;
the first remainder we compute will be the last digit in the sequence.
At the end of the iteration, the binary digits are popped from the stack and appended to the right-hand end of a string,
that will represent the number in base 2.

Check, complete and run the following ActiveCode.

.. activecode:: decimal-binary-convertion
   :language: python
   :caption: Convertion of a decimal number into binary representation

   from pythonds.basic import Stack

   def dectobin(decnumber):
     remstack = Stack()
     while decnumber > 0:
       remstack.push(decnumber % 2)
       decnumber = decnumber // 2

     binnumber = ""
     while not remstack.isEmpty():
       binnumber = binnumber + str(remstack.pop())
     return binnumber

   print(dectobin(51))




**Exercise 2.2**

A palindrome is a string that reads the same forward and backward (for example, radar, madam, or madam I'm Adam).
Write an algorithm to input a string of characters and check whether it is a palindrome.

The solution to this problem will use a deque to store the characters of the string.
We add each new character to the rear of the deque;
then, we compare the front of the deque (i.e., the first character of the string)
and the rear of the deque (i.e., the the last character of the string), and continue if they match.
Finally, we run out of characters or we have only one in the deque, depending on the length of the original
string (even or odd, respectively). In both cases, the string must be a palindrome.

Check, complete and run the code in the following ActiveCode.

.. activecode:: palindrome-checker
   :language: python
   :caption: Checking whether a string is a palindrome or not

   from pythonds.basic import Deque

   def palcheck(aString):
     word = Deque()
     for c in aString:
       word.addRear(c)

     equal = True
     while word.size() > 1 and equal:
       first = word.removeFront()
       last = word.removeRear()
       if first != last:
         equal = False
     return equal

   print(palcheck("lsdkjfskf"))
   print(palcheck("radar"))



**Exercise 2.3**

*Postfix notation* is an unambiguous way of writing an arithmetic expression
without parentheses. If *(exp1) op (exp2)* is a *infix* expression whose operator is *op*,
the postfix version of this is *pexp1 pexp2 op*, where *pexp1* is the postfix version of
*exp1* and *pexp2* is the postfix version of *exp2*.
For example, the postfix version of \\( ((3+2) ∗ (5−3))/2\\) is \\(3 2 + 5 3 − ∗ 2 / \\).

In order to provide an algorithm that evaluates an expression that is written in postfix notation,
note that whenever an operator is seen on the input, the two most recent
operands will be used in the evaluation.
For example, consider the postfix expression \\(3 2 5 * +\\)
(that is, the infix expression \\( 3 + 2 * 5 \\) ).

As you scan the expression from left to right, you first encounter the operands 3, 2, and 5, 
and you place each of them on a stack, making them available if an operator comes next.
When an operator is met (``*``, in this example) the two most recent operands
need to be used in a multiplication operation. By popping the stack twice,
we can get the proper operands and then perform the multiplication
(in this case getting the result 10).
We can now push this result onto the stack so that it can be used as an operand
for the later operators in the expression.
When the final operator is processed, there will be only one value left on the stack,
the value of the expression. In what follows, we show a pseudocode of this algorithm::

 Create an empty stack S
 Scan the expression from left to right
     if the element is an operand
       push it onto S;
     if the element is an operator, *, /, +, or -
       pop from S twice
       perform the arithmetic operation
       push the result back onto S
 When the expression ends, the result is on the stack.

Complete and run the following ActiveCode, in which the definition of a function
for the evaluation of a postfix expression is partially given.
To assist with the arithmetic, a helper function ``doMath`` is defined that will take two operands
and an operator and then perform the proper arithmetic operation.

.. activecode:: postfix-evaluation
   :language: python
   :caption: Evaluation of a postfix expression using a stack

   from pythonds.basic import Stack

   def postfixEval(postfixExpr):
     operandStack = Stack()
     tokenList = postfixExpr.split()

     for token in tokenList:
       if token in "0123456789":
         operandStack.push(int(token))
       else:
         operand2 = operandStack.pop()
         operand1 = operandStack.pop()
         result = doMath(token,operand1,operand2)
         operandStack.push(result)
     return operandStack.pop()

   def doMath(op, op1, op2):
     if op == "*":
       return op1 * op2
     elif op == "/":
       return op1 / op2
     elif op == "+":
       return op1 + op2
     else:
       return op1 - op2

   print(postfixEval('7 8 + 3 2 + /'))













Section 3. Exercises and self-evaluation
----------------------------------------

**Exercise 3.1**

What values are returned when the following series of operations are executed on an empty stack?
``push(5)``, ``push(3)``, ``pop()``, ``push(2)``, ``push(8)``, ``pop()``, ``pop()``,
``push(9)``, ``push(1)``, ``pop()``, ``push(7)``, ``push(6)``, ``pop()``, ``pop()``, ``push(4)``, ``pop()``, ``pop()``.
(Hint: Use one of the previous implementation of the stack ADT)

**Exercise 3.2**

What is the current size of a stack S, initially empty, after the execution of 25 ``push``,
12 ``top``, and 10 ``pop``, 3 of which raised ``Empty`` errors that were caught and ignored?
(Hint: Use one of the previous implementation of the stack ADT)

**Exercise 3.3**

Write a function ``transfer(S,T)`` that transfers the elements from stack S to stack T,
so that the element that at the top of S is the first to be inserted onto T,
and the element at the bottom of S ends up at the top of T.
(Hint: Transfer items one at a time)

**Exercise 3.4**

Write a recursive method that removes all the elements from a stack.

**Exercise 3.5**

Write a function that reverses a list of elements.
(Hint: Push the elements of the list onto a stack in one order, and pop and write them back to the list)

**Exercise 3.6**

What values are returned when the following series of operations are executed on an empty queue?
``enqueue(5)``, ``enqueue(3)``, ``dequeue()``, ``enqueue(2)``, ``enqueue(8)``, ``dequeue()``, ``enqueue(9)``,
``enqueue(1)``, ``dequeue()``, ``enqueue(7)``, ``enqueue(6)``, ``dequeue()``, ``dequeue()``,
``enqueue(4)``, ``dequeue()``, ``dequeue()``.
(Hint: Use one of the previous implementation of the queue ADT)

**Exercise 3.7**

What is the current size of a queue Q, initially empty, after the execution of 32 ``enqueue``,
10 ``first``, and 15 ``dequeue``, 5 of which raised ``Empty`` errors that were caught and ignored?
(Hint: Use one of the previous implementation of the queue ADT)

**Exercise 3.8**

What values are returned during the following sequence of operations are executed on an empty deque?
``add_first(4)``, ``add_last(8)``, ``add_last(9)``, ``add_first(5)``, ``back()``, ``delete_first( )``,
``delete_last( )``, ``add_last(7)``, ``first( )``, ``last( )``, ``add_last(6)``, ``delete_first( )``, ``delete_first( )``.
(Hint: Use one of the previous implementation of the deque ADT)

**Exercise 3.9**

A deque D contains the numbers (1,2,3,4,5,6,7,8), in this order, and
the queue Q is initially empty. Write a code fragment that uses only D and Q
and results in D storing the elements in the order (1,2,3,5,4,6,7,8).
(Hint: Use the return value of a removal method as a parameter to an insertion method)

**Exercise 3.10**

Repeat the previous exercise using a stack S instead of the queue Q.

**Exercise 3.11**

Given three distinct integers into a stack S, in random order,
write a piece of pseudo-code (with no loops or recursion) that uses only one comparison
and only one variable x, and that results in variable x storing the largest of the three integers.
(Hint: Pop the top integer, and remember it)

**Exercise 3.12**

Given three nonempty stacks R, S, and T, describe a sequence of operations that results in S storing all elements
originally in T below all of S’s original elements, with both sets of elements in their original order.
For example, if R = [1,2,3], S = [4,5], and T = [6,7,8,9], initially,
the final configuration should have S = [6,7,8,9,4,5].
(Hint: Use R as temporary storage, without popping its original contents)

**Exercise 3.13**

Describe how to implement the stack ADT using a single queue as an instance variable,
and only constant additional local memory within the method bodies.
(Hint: Rotate elements within the queue)

**Exercise 3.14**

Describe how to implement the queue ADT using two stacks as instance variables.
(Hint: Use one stack to collect incoming elements, and the other as a buffer for elements to be delivered)

**Exercise 3.15**

Describe how to implement the double-ended queue ADT using two stacks as instance variables.
(Hint: Use one stack for each end of the deque)

**Exercise 3.16**

Given a stack S containing \\(n)\\ elements and a queue Q, initially empty,
show how to use Q to scan S to see if it contains a certain element \\(x\\);
the algorithm must return the elements back to S in their original order.

**Exercise 3.17**

Give a complete ``ArrayDeque`` implementation of the double-ended queue ADT.

**Exercise 3.18**

Stacks can be used to provide “undo” support in applications like a Web browser or text editor.
This functionality can be implemented with an unbounded stack,
but many applications support it with a fixed-capacity stack, as follow:
when ``push`` is invoked with the stack at full capacity,
the pushed element is inserted at the top,
while the oldest element is leaked from the bottom of the stack to make room.
Give an implementation of such a ``Stack``, using a circular
array with appropriate storage capacity.
