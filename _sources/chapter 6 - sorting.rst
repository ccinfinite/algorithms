
Chapter Six: Sorting
********************

Section 1. Introduction
-----------------------

Given a collection of elements, and supposed that there exists (into the language) an order over them,
*sorting* means to rearrange them so that they are ordered from smallest to largest.
In Python, the default order of objects is defined using the \\( < \\) operator, with the property of being
irreflexive ( \\(k\\) is not less than itself) and transitive
(if \\(k_1 < k_2\\) and \\(k_2 < k_3\\), then \\(k_1 < k_3\\) ).
Moreover, Python has built-in functions for sorting data, in the form of the ``sort`` method of
the ``list`` class (that rearranges the contents of a list), and the built-in ``sorted`` function
that produces a new list containing the elements of an arbitrary collection in sorted
order. Those built-in functions use advanced algorithms, and they are highly optimized.

A programmer typically uses the built-in sorting functions, as it is unusual to have such a special
request that forces him to implement a new and different sorting algorithm.
However, it is important to understand how the sorting algorithms are built, in order to
evaluate their performances.

Sorting is one of the most studied problem in computer science;
data sets are often stored in sorted order, for example, to allow for efficient searches
with the binary search algorithm. Many advanced algorithms for a variety of problems rely on sorting as a subroutine.


Section 2. Array based sorting algorithms
-----------------------------------------
Section 2.1 Bubble sort
=======================

The *bubble sort* performs a very easy task: it makes multiple passes through a list,
comparing couples of adjacent values, and exchanging them if they are not in the correct order.
For each pass, the largest value is bubbled up to its correct location.

The Python code of the bubble sort is given below::

 1  def bubbleSort(alist):
 2    for passnum in range(len(alist)-1,0,-1):
 3      for i in range(passnum):
 4        if alist[i] > alist[i+1]:
 5          temp = alist[i]
 6          alist[i] = alist[i+1]
 7          alist[i+1] = temp

Note that if the list or array has \\(n\\) elements, there will be \\(n-1\\) comparisons during the first pass,
\\(n-2\\) during the second pass, and so on, until one comparison is needed at the final \\(n-1\\)-th pass,
with the smallest value in its correct position.
The total number of comparisons is the sum of the first \\(n-1\\) integers, that is \\((n-1)n/2\\).
This means that the algorithm has complexity in \\(O(n^2)\\).
In the best case the list is already ordered, an there will be no exchanges.
But, in the worst case, every comparison will cause an exchange, making the bubble sort a very inefficient sorting method.
Observe that if during a pass there are no exchanges, the list is sorted, already.

The following figure shows the first pass of a bubble sort.
The shaded items are compared and exchanged, if needed.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura61.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

We can write a slightly modified version of the algorithm, the *short bubble sort*,
that stops early if it finds that the list has been sorted, already::

 1  def shortbubbleSort(alist):
 2    exchanges = True
 3    passnum = len(alist)-1
 4    while passnum > 0 and exchanges:
 5      exchanges = False
 6      for i in range(passnum):
 7        if alist[i] > alist[i+1]:
 8          exchanges = True
 9          temp = alist[i]
 10         alist[i] = alist[i+1]
 11         alist[i+1] = temp
 12     passnum = passnum-1


Section 2.2 Selection sort
==========================

The *selection sort* makes multiple passes through a list of length \\(n\\),
looking for the largest value and moving it to its correct location;
this means that there is only one exchange for every pass.
After the first pass, the largest item is in the correct place.
After the second pass, the next largest is in place.
The process continues until the \\(n-1\\) pass, when the final item is in place.

The Python code of the Selection sort is given below::

 1  def selectionSort(alist):
 2    for fillslot in range(len(alist)-1,0,-1):
 3      positionOfMax=0
 4      for location in range(1,fillslot+1):
 5        if alist[location] > alist[positionOfMax]:
 6           positionOfMax = location
 7      temp = alist[fillslot]
 9      alist[fillslot] = alist[positionOfMax]
 10     alist[positionOfMax] = temp

Even if there is only one exchange for each pass, the selection sort makes the same number of comparisons as
the bubble sort; therefore, its complexity is \\(O(n^2)\\).

The following figure shows the sorting process.
On each pass, the largest remaining item is selected and placed in its proper location.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura62.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Section 2.3 Insertion sort
==========================

The *insertion sort* creates a sorted sublist (initially, only one element) in the initial positions of the list,
and properly inserts into this sublist a new item, producing a new sorted sublist.
Starting with a list with one item (the first element of the list), every item from the second to the last
is compared with those in the already sorted sublist;
the items that are greater are shifted to the right, and the current item is inserted into the sublist
when a smaller item is reached.

The Python code for the Insertion Sort is written below::

 1  def insertionSort(alist):
 2    for index in range(1,len(alist)):
 3      currentvalue = alist[index]
 5      position = index
 6      while position > 0 and alist[position-1] > currentvalue:
 8        alist[position]=alist[position-1]
 9        position = position-1
 10     alist[position]=currentvalue

In order to sort \\(n\\) items, there are \\(n-1\\) passes.
The maximum number of comparisons for an insertion sort is, again, the sum of the first \\(n-1\\) integers.
This is \\(O(n^2)\\).
In the best case, only one comparison needs to be done on each pass. This would be the case for an already sorted list.

The following figure shows the insertion sorting process.
The shaded items represent the ordered sublists for each pass.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura63.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Section 2.4 Merge sort
======================

The *merge sort* is a recursive algorithm.
If the list is empty or has one item, it is sorted (the base case).
If the list has more than one item, the list is split and the merge sort is recursively invoked on both halves.
Once the two halves are sorted, a merge is performed, taking two smaller sorted lists and combining them together
into a single, sorted, new list.

This algorithm is a classic example of *divide and conquer* strategy.
In the *divide* step, when the input size is smaller than a given threshold, the solution can be
found directly; otherwise, the input is divided in disjoint subsets.
In the *conquer* step, the subproblems associated with the subsets are solved, recursively.
In the *combine* step, the solution of the subproblems are merged to obtain a solution to the original problem.

The general algorithm for Mergesort is given below, without considering whether the sequence \\(S\\) is implemented
with an array or a linked list:

- Divide: if \\(S\\) has zero or one element, return \\(S\\);
  otherwise, split \\(S\\) in half and put the elements in two sequences, \\(S_1\\) and \\(S_2\\).
- Conquer: recursively sort sequences \\(S_1\\) and \\(S_2\\).
- Combine: put back the elements into \\(S\\) by merging the sorted sequences \\(S_1\\) and \\(S_2\\) into a sorted sequence.

The Python code for an array-based sequence is given below::

 1  def mergeSort(alist):
 2    print("Splitting ",alist)
 3    if len(alist) > 1:
 4      mid = len(alist)//2
 5      lefthalf = alist[:mid]
 6      righthalf = alist[mid:]
 7
 8      mergeSort(lefthalf)
 9      mergeSort(righthalf)
 10
 11     i=0
 12     j=0
 13     k=0
 14     while i < len(lefthalf) and j < len(righthalf):
 15       if lefthalf[i] <= righthalf[j]:
 16         alist[k] = lefthalf[i]
 17         i=i+1
 18       else:
 19         alist[k] = righthalf[j]
 20         j=j+1
 21       k=k+1
 22
 23     while i < len(lefthalf):
 24       alist[k]=lefthalf[i]
 25       i=i+1
 26       k=k+1
 27
 28     while j < len(righthalf):
 29       alist[k]=righthalf[j]
 30       j=j+1
 31       k=k+1
 32   print("Merging ",alist)

The recursive call of ``mergesort`` function is made on the left half and the right half of the list,
assuming that they have been sorted.
The rest of the function merges the two sorted lists into a larger sorted list.
This is done by placing the items into the original list ``alist`` one at a time,
taking the smallest item from the sorted lists.

The following figures show the splitting and the merging processes, respectively.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura64.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura65.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

To analyze the complexity of this algorithm, consider that the list is recursively divided in two halves:
this can be repeated for \\(\\log n\\) times, if the list has length \\(n\\).
Each couple of sublists is merged in a single list, and this requires \\(n\\) operations,
since each item in the sublists has to be processed.
Hence, the overall cost of the algorithm is \\(O(n \\log n)\\).
Note that Mergesort requires space to store the sublists extracted with the slicing operations.
This additional requirement can make this algorithm critical when applied on large data sets.


Section 2.5 Quicksort
=====================

The *quick sort* uses the divide and conquer approach and, with respect to mergesort, does not use additional space
to store the sublists extracted with the slicing operations.
As a trade-off, it may happen that the two sublists don't have the same length, causing the algorithm to slow down.
Quicksort starts selecting a value, namely the *pivot*, that is the first item in the list, usually;
its correct position in the list is found, and this position (the *split point*) is used in the process
of splitting the list in two sublists. Quicksort will be recursively applied on these sublists.

More precisely, once the pivot is selected, two marks (*leftmark* and *rightmark*) are defined
as the first and the last position of the remaining items in the list, respectively.
Leftmark is incremented until a value greater than the pivot is found;
then, rightmark is decremented until a value that is lower than the pivot is found.
The two values are out of place with respect to the split point, and they are exchanged.

This procedure continues until rightmark becomes lower than leftmark; now, the split point has been found.
The pivot value is exchanged with the contents of the split point, meaning that the pivot is in its correct position.
In the following figure the correct split point for 54 is found, and the two values are exchanged.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura66.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Note that the items to the left of the split point are lower than the pivot value,
and the items to the right of the split point are greater than the pivot value.
The list is divided at the split point and the Quicksort is invoked recursively on the two sublists.

The quickSort function is defined as follows::

 1  def quickSort(alist,first,last):
 2    if first < last:
 3      splitpoint = partition(alist,first,last)
 4      quickSort(alist,first,splitpoint-1)
 5      quickSort(alist,splitpoint+1,last)
 6
 7  def partition(alist,first,last):
 8    pivotvalue = alist[first]
 9    leftmark = first+1
 10   rightmark = last
 11
 12   done = False
 13   while not done:
 14     while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
 15       leftmark = leftmark + 1
 16     while leftmark <= rightmark and alist[rightmark] >= pivotvalue:
 17       rightmark = rightmark -1
 18
 19     if rightmark < leftmark:
 29       done = True
 20     else:
 21       temp = alist[leftmark]
 22       alist[leftmark] = alist[rightmark]
 23       alist[rightmark] = temp
 24
 25   temp = alist[first]
 26   alist[first] = alist[rightmark]
 27   alist[rightmark] = temp
 28   return rightmark

If the partition occurs always in the middle of the list, there will again be \\(\\log n\\) splittings,
for a list of length \\(n\\).
In order to find the split point, each of the \\(n\\) items needs to be compared with the pivot value.
This implies that the overall steps are \\(O(n \\log n)\\). Moreover, no additional memory is used.

The split points are not always found in the middle of the list;
in the worst case, the partitioning function divides a list of \\(n\\) items into a list of \\(1\\) item
and a list of \\(n-1\\) items. Then, the list of \\(n-1\\) items is divided into a list of \\(1\\) item
and a list of \\(n-2\\) items, respectively. This leads to a \\(O(n^2)\\) number of steps.



Section 3. Linear time sorting
------------------------------

It has been proven that \\(O(n \\log n)\\) time is necessary to sort a sequence of \\(n\\)
elements with a comparison-based sorting algorithm.
A natural question is whether there are sorting algorithms that run faster;
such algorithms exist, but they require special restrictions about the sequence to be sorted.

Section 3.1 Bucket sort
=======================

Given a sequence \\(S\\) of \\(n\\) items, whose keys are integers in the range \\([0,N -1]\\),
for \\(N \\geq 2\\), suppose that \\(S\\) has to be sorted according to the keys of the entries.
In this case, it is possible to write an algorithm to sort \\(S\\) within \\(O(n+N)\\) time.
Hence, we can achieve linear time complexity for a sorting algorithm; this depends on the
restriction made on the elements.

The bucket sort is not based on comparisons, but on using keys as indices into a *bucket array* \\(B\\)
that has cells indexed from \\(0\\) to \\(N-1\\). An entry with key \\(k\\) is placed in the  bucket \\(B[k]\\),
which itself is a sequence of entries with key \\(k\\).
After each entry of the input sequence \\(S\\) has been placed into its bucket, they are put back into \\(S\\) in sorted order,
simply by enumerating the contents of the buckets \\(B[0], B[1], \\ldots, B[N -1]\\).
The pseudo-code for the bucket sort is as follows::

 1  BucketSort(S):
 2  Input: sequence S of entries with integer keys in the range [0,N-1]
 3  Output: sequence S sorted in increasing order of the keys
 4  B is an array of N sequences, initially empty
 5
 6  for each entry e in S do
 7    k = the key of e
 8    remove e from S and insert it at the end of bucket B[k]
 9  for i = 0 to N-1 do
 10   for each entry e in sequence B[i] do
 11     remove e from B[i] and insert it at the end of S

Bucket sort runs in \\(O(n+N)\\) time and uses the same amount of space;
this means that it is efficient only when the range \\(N\\) of values for the keys is small compared
to the sequence size \\(n\\), say \\(N = O(n)\\) or \\(N = O(n \\log n)\\).
An important property of the bucket sort algorithm is that it works correctly
even if there are different elements with the same key.
In particular, it shows the property of being stable.
Let \\(S = ((k_0,v_0), \\ldots , (k_{n-1},v_{n-1}))\\) be a sequence of entries;
a sorting algorithm is *stable* if, for any couple \\((k_i,v_i)\\) and \\((k_j ,v_j)\\) of
\\(S\\) such that \\(k_i = k_j\\), and \\((k_i,v_i)\\) precedes \\((k_j ,v_j)\\) in \\(S\\) before sorting,
we have that \\((k_i,v_i)\\) precedes \\((k_j ,v_j)\\) after sorting.
Stability is important for a sorting algorithm because applications may want to preserve
the initial order of elements with the same key.

The informal description of bucket sort given previously guarantees stability
as long as we ensure that all sequences act as queues, with elements processed
and removed from the front of a sequence and inserted at the back.


Section 3.2 Radix sort
======================

Consider the general case in which we want to sort entries with keys that are pairs \\( (k, l)\\),
where \\(k\\) and \\(l\\) are integers in the range \\([0,N-1]\\), for \\(N \\geq 2\\).
An order on these keys can be defined by using the lexicographic convention,
where \\((k_1, l_1) < (k_2, l_2)\\) if \\(k_1 < k_2\\) or if \\(k_1 =k_2\\) and \\(l_1 < l_2\\).

The radix sort algorithm sorts a sequence \\(S\\) of entries with keys that are pairs,
by applying a stable bucket sort on the sequence twice; first, using one component of the pair as the key
when ordering, and then using the second component.
One problem arises: which order is correct?
That is, should we sort on the the first component and then on the second one, or viceversa?

For example, consider the  sequence
\\(S = ((3,3), (1,5), (2,5), (1,2), (2,3), (1,7), (3,2), (2,2))\\).
If we sort \\(S\\) on the first component, we get the sequence
\\(S_1 = ((1,5), (1,2), (1,7), (2,5), (2,3), (2,2), (3,3), (3,2))\\).
If we then sort \\(S_1\\) using the second component, we get the sequence
\\(S_{1,2} = ((1,2), (2,2), (3,2), (2,3), (3,3), (1,5), (2,5), (1,7))\\),
which is clearly not a sorted sequence. On the other hand, repeating the process on the
second and first component, respectively, we get the final sequence
\\(S_{2,1} = ((1,2), (1,5), (1,7), (2,2), (2,3), (2,5), (3,2), (3,3))\\),
which is lexicographically ordered.

This result can be extended to the general case; sorting the sequence by the second component and then again by
the first component, we guarantee that if two entries are equal in the second sort (by the first component),
then their relative order in the starting sequence (which is sorted by the second component) is preserved.

In general, given a sequence \\(S\\)  of \\(n\\) key-value pairs, each of which has a key
\\((k_1,k_2, \\ldots ,k_d)\\), where \\(k_i\\) is an integer in the range \\([0,N -1]\\) for \\(N \\geq 2\\),
\\(S\\) can be sorted lexicographically in time \\(O(d(n+N))\\) using radix sort.
Radix sort can be applied to any composite key. For example, we can use it to sort streings of character of moderate
length, as each individual character can be represented as an integer value.


Section 4. The best sorting algorithm
----------------------------------------

There exist several algorithms to sort a sequence. Among them, the insertion, bubble, or selection sort
have average- and worst-case complexity \\(O(n^2)\\), making them poorly suitable to be used in real scenarios.
Other methods, like merge and quicksort, have \\(O(n \\log n)\\) time complexity (this is also the optimal case
complexity of the generic sorting problem).
For specialized types of keys, there are algorithms that run in linear time, like bucketsort and radixsort.

In general, there are many factors to consider when evaluating how good a algorithm is;
time complexity is the most important, but there are always trade-offs with efficiency, memory usage, and stability.
For instance, the running time of the insertion sort can be \\(O(n+m)\\), where \\(m\\) is the number of inversions
(i.e., the number of pairs not sorted) into the sequence.
Sequences with a low number of inversions can be sorted efficiently by insertion sort.

As for quick sort, its time complexity is \\(O(n \\log n)\\) when the list is partitioned
in two sublists of the same length; unfortunately, we cannot guarantee that this happens everytime, and
the wort-case complexity is still \\(O(n^2)\\); moreover, quick sort is not a stable method, due to the swapping
of items during the partitioning of the list.
In spite of all these issues, it is considered the best choice for a general sorting algorithm;
for instance, it is used in C's libraries, in Unix operating system, and in several Java's versions.

As for merge sort, the worst-case complexity is \\(O(n \\log n)\\), but this is not a *in-place* method,
because of the extra-memory requirements in order to allocate temporary arrays, and copying between the arrays.
Merge sort isn't so attractive when compared to an in-place algorithm as quick-sort, but
it's an excellent option when the input is stratified across various levels of the computer’s
memory hierarchy (e.g., cache, main memory, external memory). In this case,
the way that merge-sort processes runs of data in long merge streams makes the best
use of all the data brought as a block into a level of memory, reducing the total number of memory transfers.
Recent versions of the Linux operating system uses a multiway merge sort.
The standard sort method of Python’s list class and of arrays in Java 7 is essentially a merge sort.

Finally, if we have to sort entries with small integer keys or character strings, bucket sort or radix sort is
an excellent choice, for it runs in \\(O(d(n+N))\\) time, where \\([0,N-1]\\) is the range of
integer keys (and \\(d = 1\\) for bucket sort). Thus, if \\(d(n+N)\\) is  below
the \\(n \\log n\\) function, then this sorting method runs faster than the others.

Section 5. Selection
--------------------

*Selection* is the problem of selecting the \\(k\\)-th smallest element from an unsorted collection of \\(n\\) elements.
An immediate solution is to sort the collection and then indexing into the sequence at index \\(k-1\\).
This requires time \\(O(n \\log n)\\).

The problem can be solved in time \\(O(n)\\) by means of a technique known as *prune-and-search*.
In this case, a problem defined on a collection of \\(n\\) objects is solved by pruning a part of the objects,
and by solving the smaller problem, recursively.
Then, when a problem defined on a constant-sized collection of objects is met,
it can be solved by means of some direct method.
Returning back to all the recursive calls completes the solution.
For instance, the *binary search* is an example of the prune-and-search technique.

The *randomized quick-select* is an application of the prune-and-search method in order to solve the selection problem.
Given an unsorted sequence \\(S\\) of \\(n\\) comparable elements, and an integer \\(k\\) in \\([1,n]\\),
pick a pivot element from \\(S\\) at random and use it to subdivide \\(S\\) into three subsequences
\\(L\\), \\(E\\), and \\(G\\), that contain the elements of \\(S\\) less than, equal to, and greater than the pivot,
respectively.
In the prune step, it is found which of the three subsets contains the desired element, based on the value
of \\(k\\) and on the sizes of those subsets. Then, recursion is applied on the appropriate subset, noting
that the rank of the desired element in the subset may differ from its rank in the full set.

An implementation of randomized quick-select is shown below::

 1  def quickselect(S, k):
 2    # returns the k-th smallest element of S, for k from 1 to len(S)
 3    if len(S) == 1:
 4      return S[0]
 5    pivot = random.choice(S)            # pick random pivot element from S
 6    L = [x for x in S if x < pivot]     # elements less than pivot
 7    E = [x for x in S if x == pivot]    # elements equal to pivot
 8    G = [x for x in S if pivot < x]     # elements greater than pivot
 9    if k <= len(L):
 10     return quickselect(L, k)     # the k-th smallest is in L
 11   elif k <= len(L) + len(E):
 12     return pivot                 # the k-th smallest is equal to pivot
 13   else:
 14     j = k-len(L)-len(E)
 15     return quickselect(G, j)     # the k-th smallest is the j-th in G

By means of a probabilistic argument (that we don't show), this algorithm runs in \\(O(n)\\) time,
over all possible random choices made by the algorithm.
In the worst case, randomized quick-select runs in \\(O(n^2)\\) time.
