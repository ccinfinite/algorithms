
Chapter Six: Sorting - exercises and challenges
***********************************************

Section 1. Reviewing the theory
-------------------------------

Complete and execute the pieces of code in the following sections (the code can be found in Chapter Six).

**Exercise 1.1**

The *bubble sort* compares couples of adjacent values in a list, and exchanges them if they are not in the correct order.
It performs multiple passes on the list; for each pass, the largest value is bubbled up to its correct location.
The Python code of the bubble sort is given in the following ActiveCode.

.. activecode:: bubble-sort
   :language: python
   :caption: Function for the bubble sort

   def bubbleSort(alist):
     for passnum in range(len(alist)-1,0,-1):
       for i in range(passnum):
         if alist[i] > alist[i+1]:
           temp = alist[i]
           alist[i] = alist[i+1]
           alist[i+1] = temp

        # INSERT NEW CODE HERE


We can write a *short bubble sort*, that stops if it finds that the list has been sorted, already.

.. activecode:: short-bubble-sort
   :language: python
   :caption: Function for the short bubble sort

   def shortbubbleSort(alist):
     exchanges = True
     passnum = len(alist)-1
     while passnum > 0 and exchanges:
       exchanges = False
       for i in range(passnum):
         if alist[i] > alist[i+1]:
           exchanges = True
           temp = alist[i]
           alist[i] = alist[i+1]
           alist[i+1] = temp
       passnum = passnum-1

        # INSERT NEW CODE HERE


**Exercise 1.2**

The *selection sort* makes multiple passes on a list,
looking for the largest value, and moving it to its correct location;
this means that there is only one exchange for every pass.
After the first pass, the largest item is in the correct place;
after the second pass, the next largest is in place.
The Python code of the Selection sort is given in the following ActiveCode.

.. activecode:: selection-sort
   :language: python
   :caption: Function for the selection sort

   def selectionSort(alist):
     for fillslot in range(len(alist)-1,0,-1):
       positionOfMax=0
       for location in range(1,fillslot+1):
         if alist[location] > alist[positionOfMax]:
           positionOfMax = location
       temp = alist[fillslot]
       alist[fillslot] = alist[positionOfMax]
       alist[positionOfMax] = temp

        # INSERT NEW CODE HERE


**Exercise 1.3**

The *insertion sort* creates a sorted sublist in the initial positions of the list,
and inserts into this sublist a new item, producing a new sorted sublist.
Starting with a list with the first item only, every item from the second to the last
is compared with those in the already sorted sublist;
the items that are greater are shifted to the right, and the current item is inserted into the sublist
when a smaller item is reached. The Python code for the Insertion Sort is written in the following ActiveCode.

.. activecode:: insertion-sort
   :language: python
   :caption: Function for the insertion sort

   def insertionSort(alist):
     for index in range(1,len(alist)):
       currentvalue = alist[index]
       position = index
       while position > 0 and alist[position-1] > currentvalue:
         alist[position]=alist[position-1]
         position = position-1
       alist[position]=currentvalue

        # INSERT NEW CODE HERE


**Exercise 1.4**

The *merge sort* is a recursive algorithm.
If the list is empty or has one item, it is sorted; if the list has more than one item,
the list is split and the merge sort is recursively invoked on both halves.
Once the two halves are sorted, a merge is performed.
The Python code for an array-based sequence is given in the following ActiveCode.

.. activecode:: merge-sort
   :language: python
   :caption: Function for the merge sort

   def mergeSort(alist):
     print("Splitting ",alist)
     if len(alist) > 1:
       mid = len(alist)//2
       lefthalf = alist[:mid]
       righthalf = alist[mid:]

       mergeSort(lefthalf)
       mergeSort(righthalf)

       i=0
       j=0
       k=0
       while i < len(lefthalf) and j$< len(righthalf):
         if lefthalf[i] <= righthalf[j]:
           alist[k] = lefthalf[i]
           i=i+1
         else:
           alist[k] = righthalf[j]
           j=j+1
         k=k+1

       while i < len(lefthalf):
         alist[k]=lefthalf[i]
         i=i+1
         k=k+1

       while j <$len(righthalf):
         alist[k]=righthalf[j]
         j=j+1
         k=k+1
     print("Merging ",alist)

        # INSERT NEW CODE HERE


**Exercise 1.5**

The *quick sort* starts selecting a value, the *pivot*, that is the first item in the list, usually;
its correct position in the list is found, and this position (the *split point*) is used in the process
of splitting the list in two sublists. Quicksort will be recursively applied on these sublists.

Two marks, *leftmark* and *rightmark*, are defined
as the first and the last position of the remaining items in the list, respectively.
Leftmark is incremented until a value greater than the pivot is found;
rightmark is decremented until a value that is lower than the pivot is found.
The two values are out of place with respect to the split point, and they are exchanged.

This procedure continues until rightmark becomes lower than leftmark; now, the split point has been found.
The pivot value is exchanged with the contents of the split point, meaning that the pivot is in its correct position.
Quicksort is now invoked recursively on the two sublists. The quickSort function is defined as follows.

.. activecode:: quick-sort
   :language: python
   :caption: Function for the quick sort

   def quickSort(alist,first,last):
     if first < last:
       splitpoint = partition(alist,first,last)
       quickSort(alist,first,splitpoint-1)
       quickSort(alist,splitpoint+1,last)

   def partition(alist,first,last):
     pivotvalue = alist[first]
     leftmark = first+1
     rightmark = last

     done = False
     while not done:
       while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
         leftmark = leftmark + 1
       while leftmark <= rightmark and alist[rightmark] >= pivotvalue:
         rightmark = rightmark -1

       if rightmark < leftmark:
         done = True
       else:
         temp = alist[leftmark]
         alist[leftmark] = alist[rightmark]
         alist[rightmark] = temp

     temp = alist[first]
     alist[first] = alist[rightmark]
     alist[rightmark] = temp
     return rightmark

        # INSERT NEW CODE HERE


**Exercise 1.6**

Selection is the problem of selecting the \\(k\\)-th smallest element from an unsorted collection of \\(n\\) elements.

Given an unsorted sequence \\(S\\) of \\(n\\) comparable elements, and an integer \\(k\\) in \\([1,n]\\),
the *randomized quick-select* picks a pivot element from \\(S\\) at random,
and use it to subdivide \\(S\\) into three subsequences
\\(L\\), \\(E\\), and \\(G\\), that contain the elements of \\(S\\) less than, equal to, and greater than the pivot,
respectively.
Then, it is found which of the three subsets contains the desired element, based on the value
of \\(k\\) and on the sizes of those subsets. Then, recursion is applied on the appropriate subset.
An implementation of randomized quick-select is shown below.

.. activecode:: quick-sort-2
   :language: python
   :caption: Function for the quick sort

   def quickselect(S, k):
     if len(S) == 1:
       return S[0]
     pivot = random.choice(S)
     L = [x for x in S if x < pivot]     # elements less than pivot
     E = [x for x in S if x == pivot]    # elements equal to pivot
     G = [x for x in S if pivot < x]     # elements greater than pivot
     if k <= len(L):
       return quickselect(L, k)     # the k-th smallest is in L
     elif k <= len(L) + len(E):
       return pivot                 # the k-th smallest is equal to pivot
     else:
       j = k-len(L)-len(E)
       return quickselect(G, j)     # the k-th smallest is the j-th in G

        # INSERT NEW CODE HERE



Section 2. Problems on sorting
------------------------------

In this section, we will show some classical algorithm on sorting; check, complete and run the ActiveCode in each subsection.

**Exercise 2.1**

*Pigeonhole sort* is an algorithm that is suitable for sorting sequences of elements where the number of elements
and the number of possible key values are approximately the same.
It requires \\( O(n+R) \\) time, where \\(n\\) is the number of elements in the sequence and \\(R\\), the *range*,
is the number of possible values.

The algorithm finds minimum and maximum values in the sequence (*min* and *max*, respectively),
and the range \\( R=max-min+1 \\).
A new array of *pigeonholes* is set up, initially empty; its size is equal to the range \\( R \\).

Each element \\(arr[i]\\) of the initial array is put in the hole at index \\(arr[i] – min\\).
Then, the elements of the pigeonhole array, non-empty holes, are put back into the original array.
Pigeonhole sort has limited use as requirements are rarely met.
For arrays where range is much larger than \\(n\\), bucket sort is a generalization that is more efficient in space and time.
An implementation of pigeonhole sort is shown in the following ActiveCode.

.. activecode:: pigeonhole-sort
   :language: python
   :caption: Function for the pigeonhole sort

   def pigeonhole_sort(a):
     # building the pigeonholes
     my_min = min(a)
     my_max = max(a)
     size = my_max - my_min + 1
     holes = [0] * size

     for x in a:
       holes[x - my_min] += 1

     i = 0
     for count in range(size):
       while holes[count] > 0:
         holes[count] -= 1
         a[i] = count + my_min
         i += 1

        # INSERT NEW CODE HERE


**Exercise 2.2**

*Comb sort* is an improvement of Bubble Sort. Bubble sort compares adjacent values and removes all inversions.
Comb Sort uses gaps of size more than 1. The gap starts with a large value and shrinks by a factor of 1.3 in every iteration,
until it reaches the value 1. Thus, Comb Sort removes more than one *inversion count* with one swap.

The shrink factor has been empirically found to be 1.3; on average case, this algorithm performs better than Bubble Sort.
The average case time complexity of the algorithm is \\( \\Omega(n^2/2^p) \\), where \\(p\\) is the number of increments.
The worst-case complexity of this algorithm is \\(O(n^2)\\) and the Best Case complexity is \\(O(n \\log n)\\).
Below is the implementation.

.. activecode:: comb-sort
   :language: python
   :caption: Function for the comb sort

   # find next gap from current
   def getNextGap(gap):
     gap = (gap * 10) // 13
     if gap < 1:
       return 1
     return gap

   # sort arr[] using Comb Sort
   def combSort(arr):
     n = len(arr)
     gap = n

     swapped = True
     while gap !=1 or swapped == 1:
       gap = getNextGap(gap)        # find next gap
       swapped = False               # initialize swapped as false, to check if a swap happens
       for i in range(0, n-gap):
         if arr[i] > arr[i + gap]:
           arr[i], arr[i + gap]=arr[i + gap], arr[i]
           swapped = True

         # INSERT NEW CODE HERE



**Exercise 2.3**

*Jump search* is a searching algorithm for sorted arrays.
It checks fewer elements (than linear search) by jumping ahead by fixed steps or skipping some elements,
instead of searching all elements.
Suppose we have an array \\(arr[]\\) of size \\(n\\) and block (to be jumped) of size \\(m\\).
We search at the indexes \\(arr[0] , arr[m] , arr[2m], \\ldots , arr[km] , \\ldots \\), and so on.
Once we find the interval \\( arr[km] < x < arr[(k+1)m] \\), we perform a linear search operation from the index
\\(km\\) to find the element \\(x\\).

In the worst case, we have to do \\(n/m\\) jumps, and if the last checked value is greater than
the element to be searched for,
we perform \\(m-1\\) comparisons, for the linear search.
Therefore, the total number of comparisons in the worst case will be \\( ((n/m) + m-1) \\).
The function \\( f(m)=(n/m) + m-1 \\) is minimum when \\( m = \\sqrt n \\); therefore, the best step size is
\\( m = \\sqrt n \\).
In what follows, the code to implement Jump search.

.. activecode:: jump-search
   :language: python
   :caption: Function for the jump search

   import math

   def jumpSearch(arr , x):
     n = len(arr)
     step = math.sqrt(n)

     # finding the block where the element is
     prev = 0
     while arr[int(min(step, n)-1)] < x:
       prev = step
       step += math.sqrt(n)
       if prev >= n:
         return -1

     # linear search for x in the block beginning with prev
     while arr[int(prev)] < x:
       prev += 1
       if prev == min(step, n):   # if we reach next block or end of array, the element is not present
         return -1

     # if element is found
     if arr[int(prev)] == x:
       return int(prev)
     return -1

        # INSERT NEW CODE HERE



**Exercise 2.4**

The *Exponential search* on a sorted array involves two steps::

 Find range where element x is present
 Do Binary Search in that range

In order to find the range, we start with subarray of size 1, compare its last element with \\(x\\);
then try size 2, then 4, and so on until last element of a subarray is not greater than \\(x\\).
Once we find an index \\(i\\), we know that the element must be present between \\(i/2\\) and \\(i\\).
The time complexity is \\( O(\\log n) \\), and the space required by the binary search \\( O(\\log n) \\) space.
With iterative Binary Search, we would need only \\( O(1)\\) space.

.. activecode:: jump-search-2
   :language: python
   :caption: Function for the jump search

   def binarySearch(arr, l, r, x):
   # binary search function that returns the location of x in given array arr[l..r]
     if r >= l:
       mid = l + ( r-l ) // 2
       if arr[mid] == x:
         return mid
       if arr[mid] > x:
         return binarySearch(arr, l, mid - 1, x)
       return binarySearch(arr, mid + 1, r, x)
     return -1

   def exponentialSearch(arr, x):
   # returns the position of first occurrence of x in array
     n = len(arr)
     if arr[0] == x:
       return 0

     # find range for binary search by repeated doubling
     i = 1
     while i < n and arr[i] <= x:
       i = i * 2

     # call binary search for the range
     return binarySearch(arr, i // 2, min(i, n-1), x)

        # INSERT NEW CODE HERE


Section 3. Exercises and self-evaluation
----------------------------------------

**Exercise 3.1**

Given the following list of numbers to sort: [19, 1, 9, 7, 3, 10, 13, 15, 8, 12];
which list is obtained after three complete passes of bubble sort?

**Exercise 3.2**

Given the following list of numbers to sort: [11, 7, 12, 14, 19, 1, 6, 18, 8, 20];
which list is obtained after three complete passes of selection sort?

**Exercise 3.3**

Given the following list of numbers to sort: [15, 5, 4, 18, 12, 19, 14, 10, 8, 20];
which list is obtained after three complete passes of insertion sort?

**Exercise 3.4**

Given the following list of numbers: [21, 1, 26, 45, 29, 28, 2, 9, 16, 49, 39, 27, 43, 34, 46, 40];
which is the list to be sorted after three recursive calls of mergesort?

**Exercise 3.5**

Given the following list of numbers: [21, 1, 26, 45, 29, 28, 2, 9, 16, 49, 39, 27, 43, 34, 46, 40];
which are the first two lists to be merged?

**Exercise 3.6**

Give the following list of numbers [14, 17, 13, 15, 19, 10, 3, 16, 9, 12]:
which is the list after the second partitioning of the quicksort?

**Exercise 3.7**

Which among Shell-, Quick-, Merge-, or Insertion-sort are guaranteed to be \\(O(n \\log n)\\) in the worst case?

**Exercise 3.8**

An algorithm that sorts key-value entries by key is *straggling*
if any time two entries \\(e_i\\) and \\(e_j\\) have equal keys, but \\(e_i\\) appears before \\(e_j\\) in the input,
then the algorithm places \\(e_i\\) after \\(e_j\\) in the output.
Describe a change to the merge-sort algorithm to make it straggling.

**Exercise 3.9**

Suppose we are given two \\(n\\)-element sorted sequences \\(A\\) and \\(B\\) each with distinct elements,
but with some elements in both sequences.
Find an \\(O(n)\\)-time method for computing a sequence representing the
union of \\(A\\) and \\(B\\) (with no duplicates) as a sorted sequence.

**Exercise 3.10**

Of the \\(n!\\) possible inputs to a given comparison-based sorting algorithm,
what is the maximum number of inputs that could be correctly sorted with just \\(n\\) comparisons?

**Exercise 3.11**

Suppose \\(S\\) is a sequence of \\(n\\) values, each equal to 0 or 1.
How long will it take to sort \\(S\\) with the merge-sort algorithm? And with the quick-sort?
