===================================
Algorithms
===================================

.. toctree::
   :maxdepth: 1

   chapter 1 - analysis.rst
   chapter 1 - o2 exercises.rst
   chapter 2 - recursion.rst
   chapter 2 - o2 exercises.rst
   chapter 3 - array based sequences.rst
   chapter 3 - o2 exercises.rst
   chapter 4 - sqd.rst
   chapter 4 - o2 exercises.rst
   chapter 5 - linkedlists.rst
   chapter 5 - o2 exercises.rst
   chapter 6 - sorting.rst
   chapter 6 - o2 exercises.rst
