
Chapter One: Analysis of algorithms
***************************************************************


Section 1. Introduction
-----------------------

A *data structure* consists in a formal definition of how a set of data is organized and accessed;
an *algorithm* is a procedure that is defined and used in order to perform some tasks over the data structure,
within a finite and computable amount of time and/or space.

Time and space are the resources that every model of computation uses.
We want to classify data structures and algorithms as good or bad,
according to the amount of resources consumed during the computation;
thus, we are interested in developing precise analysis tools that capture the running time
and the space usage for each algorithm.

For theoretical reasons, time and space are related, and time is considered as the most interesting resource in computer science.
Many factors can affect the time complexity of an algorithm (hardware environment, operating system, programming language, etc.),
but it is important to characterize an algorithm's running time as a function of the input size;
all the other factors being equal, the complexity of an algorithm is evaluated as a
relation between the size of the input and the running time of the algorithm itself.

In what follows, we will introduce some tools that will allow us to perform experimental studies
on the complexity of an algorithm, and we will show that using experiments
for evaluating algorithm efficiency has several limitations.
Then, we will focus on the development of a few mathematical tools that can be used to
express the relationship between the running time of an algorithm and the size of its input.


Section 2. Experimental analysis of algorithms
----------------------------------------------

An easy way of studying the complexity of an algorithm is to execute it on various significant test inputs,
and to record the time spent during each execution.
In Python, this can be done by using the  ``time()`` function of the ``time`` module.
This function reports the number of seconds elapsed since a benchmark time;
if ``time()`` is launched immediately before and after the run of the algorithm,
we are able to measure the elapsed time as the difference of the two values.

Measuring time in the previous way is a good reflection of the algorithm complexity, but it is by no means perfect.
The elapsed time will depend, for instance, on other processes running on the computer when the test is performed
(because the central processing unit is shared among various processes):
instead, a fair measure should consider statistically significant tests repeated on identical computer systems,
with identical input and algorithms.
The result of these tests could be visualized on a (input-size x running-time) chart,
providing some intuition regarding the relationship between input size and execution time for the algorithm.
This may lead to understand the best function of the input size to the experimental data.

While experimental analysis of running times is valuable, especially when fine-tuning production-quality code,
there are some limitations:

- experimental running times of two algorithms are hardly comparable,
  unless the experiments are performed in the same hardware and software environments;
- only a limited set of test inputs are available;
- in order to study the running time of an algorithms experimentally, the former has to be fully implemented
  (that is, it has to be fully translated into a specific program).

These are serious drawback to the use of experimental studies.


Section 3. Formal analysis of algorithms
----------------------------------------

With the term *formal analysis* we describe an approach for analyzing the complexity of algorithms
in a way that is independent of the hardware and software environment,
studying a high-level description of the algorithm
(instead of a complete implementation), and taking into account all possible inputs.
In order to do this, we define a set of primitive operations, and we perform an analysis
on the  combination of operations that form the algorithm
(either in the form of an actual code fragment, or language-independent pseudo-code).
The set of primitive operations includes:

- accessing to an object for assignment or reading;
- arithmetic operations or comparison;
- calling a function or method;
- accessing to a list by index;
- returning from a function.

Each primitive operation corresponds to a low-level instruction with a constant execution time.
Instead of measuring the execution time of the algorithm,
we count how many primitive operations are executed, and use this number as a measure of the running time of the algorithm
(we assume that the operation count will correlate to an actual running time in a specific computer,
given that each primitive operation corresponds to a constant number of instructions,
and that there is only a fixed number of primitive operations).

To capture the growth of an algorithm's running time, we will associate,
with each algorithm, a function \\(f(n)\\) that characterizes the number of primitive
operations that are performed as a function of the input size \\(n\\).
Some functions are extensively used in the framework of computational complexity:

- the *constant* function :math:`f(n)=c`, for a constant number \\(c\\);
- the *logarithmic* function \\( f (n) = \log_b n \\), for a constant number \\( b \\);
- the *linear* function \\( f(n)=n \\);
- the \\( f(n)= n \log n \\) function;
- the *quadratic* function \\( f(n)=n^2 \\);
- the *polynomial function* \\( f(n)=n^k \\), for a constant number \\( k \\);
- the *exponential function* \\( f(n)=b^n \\), for a constant number \\( b \\).

We provide here some hints about these functions.

**The constant function.**
For any argument \\(n\\), the constant function returns the value \\(c\\).
As simple as it is, this function is useful in algorithm analysis, because
it expresses the number of steps needed to do a basic operation on a computer,
like adding two numbers, assigning a value to some variable, or comparing two numbers.
A constant complexity is the lowest that an algorithm can achieve.

**The logarithmic function.**
This function is defined as \\(x = \log_b n\\), if and only if \\(b^x = n\\).
By definition, \\(\log_b 1 = 0\\). The constant value \\(b\\) is called the *base* of the logarithm.
The most common base for the logarithm function in computer science is 2,
as computers represent integers in binary notation, and because a common operation in many
algorithms is to repeatedly divide an input in half.
We omit the base from the notation when it is 2, meaning that \\(\log n = \log_2 n\\).
A logarithmic complexity is typical of those recursive algorithms that work on
increasingly smaller parts of the input.

**The linear function.**
Given an input value \\(n\\), the linear function returns the value \\(n\\) itself.
This function is met in algorithm analysis any time we perform a single basic operation for each of \\(n\\) elements.
For example, comparing a number \\(x\\) to each element of a sequence of size \\(n\\) requires \\(n\\) comparisons.
The linear function represents the best running time for any algorithm that processes
objects that are not already in the computer's memory (reading \\(n\\) objects requires \\(n\\) operations).
In general, algorithms with linear complexity are considered among the most efficient.

**The \\(n\log n\\) function.**
This function assigns to an input \\(n\\) the value of \\(n\\) multiplied by the logarithm base-two of \\(n\\).
The rate of growth of this function is obviously steeper than the linear function,
but it still stays below the quadratic function;
therefore, an algorithm with a running time that is proportional to \\(n \log n\\)
has to be preferred than one with quadratic running time.
Several important algorithms exhibit a running time proportional to this function.
For example, the best algorithms for sorting \\(n\\) arbitrary values require time proportional to \\(n\log n\\).

**The quadratic function.**
Given an input value \\(n\\), this function returns the product \\(n^2\\);
this function appears in the analysis of algorithms when nested loops are encountered, in which the inner
loop performs a linear number of operations, and the outer loop is performed a linear number of times.
In such cases, the algorithm performs \\(n\\) x \\(n = n^2\\) operations; for instance, some sorting algorithm have
quadratic complexity.

**The polynomial function.**
Continuing our discussion of functions that are powers of the input, we consider the polynomial function,
\\(f(n) = n^k\\), with \\(k\\) a fixed natural number. This function assigns to an input value \\(n\\) the product
of \\(n\\) with itself, \\(k\\) times.
This function is very common in the context of algorithm analysis, and represents, for theoretical reasons,
the limit of what is considered *feasible* with a computer.
However, we must notice that an algorithm with a polynomial running time
such as \\( n^\{354\} \\) is not considered feasible, generally.

**The exponential function.**
Another function used in the analysis of algorithms is the exponential function, \\(f(n)=b^n\\),
with \\(b\\) is a positive constant, called the *base*.
This function assigns to the input argument \\(n\\) the value obtained by multiplying the base \\(b\\) by itself \\(n\\) times.
As with the logarithm function, the most common base for the exponential function in algorithm analysis is 2.
For instance, if we have a loop that starts by performing one operation
and then doubles the number of operations performed with each iteration, then the
number of operations performed in the \\(n-\\)th iteration is \\(2^n\\).
The exponential function, and every function that growths similarly, is considered *unfeasible* in computer science.
If we can prove that an algorithm, in the best case, has exponential complexity,
that algorithm could be scarcely executed
in a computer, no matter how powerful the hardware is. This is due to the extremely steep growing of the function.
Most of the open problem in computer science are somehow related to the exponential complexity.

A formal analysis of the complexity of an algorithm requires that the running time is expressed as a function
of the input size; usually, the *best*-, *worst*- and *average-case* scenarios are taken into consideration.
Best-case scenario happens when the input configuration is such that the algorithm work at its best speed;
for instance, sorting a collection of data that are already sorted should require the minimum amount of time.
On the contrary, worst-case scenario happens when the algorithm has to perform every
possible step in order to achieve its goal;
imagine a collection of numbers that are required to be sorted from the least,
and are instead sorted from the biggest one.
Those two cases are way easier to evaluate than the average-case. In this case, we should be able
to express the running time of an algorithm as the function of
the input size, obtained by taking the average over all possible inputs of the same
size. This requires to define a probabilistic input distribution on the inputs,
and goes beyond our motivation here. We always consider running times in terms of the worst case,
as a function of the input size of the algorithm.


Section 4. Asymptotic analysis
------------------------------

We have already understood that, in algorithm analysis, we want to characterize the time complexity
by using functions that map the size of the input, \\(n\\), to values that correspond to
the main factors that provide the growth's rate in terms of \\(n\\).
In what follows, we will provide some notations that express this relation.

Each basic step in a description of an algorithm (as well as in its implementation)
corresponds to a fixed number of primitive operations, and we
want to count the number of primitive operations executed, up to a constant factor.

In the following piece of code, the function ``find_max`` looks for the largest element of a
Python list ``data``::

 1  def find_max(data):
 2    # Return the maximum element from a list
 3    biggest = data[0]
 4    for val in data:
 5      if val > biggest:
 6        biggest = val
 7    return biggest

This algorithm runs proportionally to \\(n\\), as the loop executes once for each ``data`` element,
and a fixed number of primitive operations (comparison and assignment) executes for each pass.

**The \\( O \\) Notation.**
Let \\(f\\) and \\(g\\) be functions mapping positive integers to positive real numbers.
We say that \\(f(n)\\) is \\( O(g(n)) \\), if there exist a real \\(c > 0\\) and an integer \\(n_0 \\geq 1\\)
such that \\(f(n) \\leq cg(n)\\), for \\(n \\geq n_0\\).
This notation is pronounced as \\(f(n)\\) is "big-Oh" of \\(g(n)\\), or \\(f(n)\\) "is order of" \\(g(n)\\),
or \\(f(n) \\in O(g(n))\\); the function \\(f\\) is “less than or equal to” another function \\(g\\)
up to a constant factor and in the asymptotic sense, as \\(n\\) grows toward infinity.

The \\(O\\) notation is used to characterize running times and space bounds of algorithms
in terms of some parameter \\(n\\), which is always intended as a measure of the size of the problem.
For instance, if \\(n\\) denotes the number of elements of the sequence ``data`` in the previous code for ``find_max``,
we will say that the algorithm has running time \\(O(n)\\). This happens because the
assignment before the loop requires a constant number of primitive operations,
and the same happens for each iteration of the loop; finally, the loop executes \\(n\\) times.
Given that each primitive operation runs in constant time,
we have that the running time of the algorithm on an input of size \\(n\\) is at most a constant times \\(n\\),
that is \\(O(n)\\).

**The \\( \\Omega \\) notation.**
Let \\(f\\) and \\(g\\) be functions mapping positive integers to positive real numbers.
We say that \\(f(n)\\) is \\( \\Omega (g(n)) \\), if \\(g(n)\\) is \\(O(f(n))\\), that is, there exist a real \\(c> 0\\)
and an integer \\(n_0 \\geq 1\\)
such that \\(f(n) \\geq cg(n)\\), for \\(n \\geq n_0\\).
This notation is pronounced \\(f(n)\\) is "big Omega" of \\(g(n)\\); the function \\(f\\) is "greater that equal to"
the function \\(g\\), up to a constant factor, and in the asymptotic sense.

**The \\( \\Theta \\) notation.**
Let \\(f\\) and \\(g\\) be functions mapping positive integers to positive real numbers.
We say that \\(f(n)\\) is \\(\\Theta (g(n))\\), if \\(f(n)\\) is \\(O(g(n))\\) and \\(f(n)\\) is \\(\\Omega (g(n))\\),
that is, there are reals \\(c' > 0\\) and \\(c'' > 0\\), and an integer \\(n_0 \\geq 1\\) such that
\\(c'g(n) \\leq f(n) \\leq c''g(n)\\), for \\(n \\geq n_0\\).
This denotes that the two functions grow at the same rate.


**Comparative analysis.**
The \\(O\\) notation can be used to order classes of functions by asymptotic rate of growth.
The previously introduced seven functions can be ordered by increasing growth rate in the following
sequence: \\(1\\), \\(\\log n\\), \\(n\\), \\(n\\log n\\), \\(n^k\\), \\(2^n\\).
This means that if \\(f(n)\\) precedes \\(g(n)\\) in the sequence, then \\(f(n)\\) is \\(O(g(n))\\).
For instance, if two algorithms solve the same problem within running time \\(O(n)\\) and \\(O(n^2)\\), respectively,
the former is asymptotically better than the latter, up to a certain value of \\(n\\).

It is interesting to compare the rates of growth of the seven function, as it appears in the table below.

======= ============= === ============== ========= ========== =============
\\(n\\) \\(\\log n\\) n   \\(n \log n\\) \\(n^2\\) \\(n^3\\)  \\(2^n\\)
======= ============= === ============== ========= ========== =============
8       3             8   24             64        512        256
16      4             16  64             256       4,096      65,536
32      5             32  160            1,024     32,768     4,294,967,296
64      6             64  384            4,096     262,144    1.84×10^19
128     7             128 896            16,384    2,097,152  3.40×10^38
256     8             256 2,048          65,536    16,777,216 1.15×10^77
======= ============= === ============== ========= ========== =============

The following table explores the maximum size allowed for an input instance that is processed
by an algorithm in 1 second, 1 minute, and 1 hour, showing that an asymptotically slow algorithm is beaten, in
the long run, by an asymptotically faster algorithm,
even if the constant factor for the asymptotically faster algorithm is much worse.

+------------------------------+-----------------------------------+
|                              | Maximum problem size ( \\(n\\) )  |
+------------------------------+------------+----------+-----------+
| Running time \\( \\mu\\)-sec | 1 second   | 1 minute | 1 hour    |
+==============================+============+==========+===========+
| \\(400n\\)                   | 2,500      | 150,000  | 9,000,000 |
+------------------------------+------------+----------+-----------+
| \\(2n^2\\)                   | 707        | 5,477    | 42,426    |
+------------------------------+------------+----------+-----------+
| \\(2^n\\)                    | 19         | 25       |    31     |
+------------------------------+------------+----------+-----------+

The following table shows the new maximum problem size achievable for any fixed amount of time,
assuming algorithms with the given running times are now run on a computer 256 times faster than the previous one.
Each entry is a function of ``m``, the previous maximum problem size.
Even if we use a faster hardware, we still cannot overcome the handicap of an asymptotically slow algorithm.

+------------------------------+---------------------------+
| Running time \\( \\mu\\)-sec | New maximum problem size  |
+==============================+===========================+
| \\(400n\\)                   | \\(256m\\)                |
+------------------------------+---------------------------+
| \\(2n^2\\)                   | \\(16m\\)                 |
+------------------------------+---------------------------+
| \\(2^n\\)                    | \\(m+8\\)                 |
+------------------------------+---------------------------+

Note that the use of the \\(O\\) notation can be misleading should the constant factors they hide be very large.
Consider a function like \\(10^{100} n\\), that is linear, but definitely unfeasible;
this happens because the constant factor \\(10^{100}\\) is too big to be handled by any computer.
Without stretching our example to this limit, we should consider that constant factors and lower-order terms
are always hiding in the complexity of an algorithm.
A "fast" algorithm, generally speaking, is any algorithm running in \\(O(n\log n)\\) time (with a reasonable
constant factor); in some context, an \\(O(n^2)\\)-time complexity is considered acceptable.
The line between efficient and inefficient algorithms has to be drawn between those algorithms running in
polynomial time \\(O(n^k)\\) (with a low \\(k\\)) and those running in exponential time \\(O(k^n)\\);
the distinction between polynomial-time and exponential-time algorithms is considered a robust measure of feasibility.


Section 5. Examples of algorithm analysis
-----------------------------------------

In this section, we provide some simple algorithms, and we show the related running time analysis
using the notations introduced before.
We will use an instance of a Python’s list class, ``data``, as a representation for an array of values.
A call to the function ``len(data)`` is evaluated in constant time, given that
a variable that records the length of the list is maintained for each list instance;
thus, a direct access to this variable will do the job within \\(O(1)\\) time.
Morover, a Python’s list class allows access to an arbitrary element of the list (using ``data[j]``, for each \\(j\\) )
in constant time. This happens because Python’s lists are implemented as array-based sequences,
and references to a list’s elements are stored in a consecutive block of memory.
The \\(j-\\)th element of the list can be found using the index as an offset into the underlying array.
Note that we could use any programming language, or any pseudo-language to describe the
algorithm, and the analysis would remain the same.

Section 5.1 Finding the maximum
================================

Given the previous considerations on ``data``, ``len``, and ``data[j]``, we can
confirm the preliminary analysis we have made regarding the ``find_ max`` algorithm,
with \\(n\\) the length of ``data``.
The initialization of ``biggest`` to ``data[0]``  uses \\(O(1)\\) time.
The loop executes \\(n\\) times, and within each iteration, it performs one comparison and possibly one assignment.
Finally, the ``return`` statement in Python uses \\(O(1)\\) time.
We then have that the function runs in \\(O(n)\\) time, as expected.

Section 5.2 Prefix averages
===========================

Let \\(S\\) be a sequence of \\(n\\) numbers; we want to compute a sequence \\(A\\),
such that \\(A[j]\\) is the average of elements \\(S[0], \\ldots ,S[j]\\), for \\(j = 0, \\ldots ,n-1\\).
This problem is called the *prefix averages of a sequence*, and has several applications.
For example, given the year-by-year returns of a mutual fund,
an investor may wish to see the fund’s average annual returns for the
most recent years.
Likewise, given the logs of Web usages, a Web site manager may wish
to track average usage trends over various periods of time.

The first implementation of an algorithm for computing prefix averages is given in the following code.
Every element of \\(A\\) is computed separately, using a loop to compute the partial sum::

 1  def prefix_average(S):
 2    # Return list A such that A[j] equals average of S[0], ..., S[j], for all j
 3    n = len(S)
 4    A = [0]*n                # list of n zeros
 5    for j in range(n):
 6      total = 0
 7      for i in range(j + 1):
 8        total += S[i]
 9      A[j] = total / (j+1)    # the j-th average
 10   return A

The assignment ``n = len(S)`` executes in constant time.
The statement ``A = [0]*n`` creates and initializes a list with length \\(n\\), with all entries equal to zero;
this uses a constant number of primitive operations per entry, and thus runs in \\(O(n)\\) time.
The body of the outer loop  is controlled by counter \\(j\\), and is executed \\(n\\) times,
for \\(j = 0, \\ldots ,n-1\\).
This means that statements ``total = 0`` and ``A[j] = total / (j+1)`` are executed \\(n\\) times each.
They contribute with a number of primitive operations proportional to \\(n\\), that is, \\(O(n)\\) time.
The body of the inner loop, which is controlled by counter \\(i\\), is executed \\(j+1\\) times,
depending on the current value of the outer loop counter \\(j\\).
Thus, the statement ``total += S[i]`` is executed \\(1+2+3+ \\ldots +n\\) times, that equals to \\(n(n+1)/2\\);
this means \\(O(n^2)\\) time.
The running time of this implementation is given by the sum of three terms:
the first and the second terms are \\(O(n)\\), and the third term is \\(O(n^2)\\).
The overall running time of ``prefix_average`` is \\(O(n^2)\\).

A linear time algorithm for the same problem is given in the following code::

 1  def prefix_average_linear(S):
 2    # Return list A such that A[j] equals average of S[0], ..., S[j], for all j
 3    n = len(S)
 4    A = [0]*n                # list of n zeros
 5    total = 0
 6    for j in range(n):
 7      total += S[j]
 8      A[j] = total / (j+1)    # average based on current sum
 9    return A

In the first algorithm, the prefix sum is computed anew for each value of \\(j\\), leading to the quadratic behavior.
In this new algorithm, we maintain the current prefix sum dynamically, effectively
computing \\(S[0]+S[1]+ \\ldots +S[j]\\) as ``total += S[j]``, where  ``total`` is equal to the
sum \\(S[0]+S[1]+\\ldots +S[ j-1]\\) computed by the previous pass of the loop over \\(j\\).
Initializing variables uses \\(O(1)\\) time.
Initializing the list uses \\(O(n)\\) time.
There is a single loop, controlled by counter \\(j\\);
the maintenance of that counter by the range iterator costs \\(O(n)\\) time.
The body of the loop is executed \\(n\\) times, for \\(j = 0, \\ldots ,n-1\\).
Thus, statements ``total += S[j]`` and ``A[j] = total / (j+1)`` are executed \\(n\\) times.
Since each of these statements uses \\(O(1)\\) time per iteration, their overall contribution is \\(O(n)\\) time.
The overall running time of the algorithm ``prefix_average_linear`` is \\(O(n)\\), which is much
better than the quadratic time of the first algorithm we have provided.


Section 5.3 Three-way set disjointness
======================================

Given three sequences of numbers, \\(A\\), \\(B\\), and \\(C\\), and given that no individual sequence contains
duplicate values, the *three-way set disjointness* problem is to decide if the intersection of the three sequences is empty.
A simple Python implementation is given below::

 1  def disjoint1(A, B, C):
 2    # Return True if there is no element in common to all three lists
 3    for a in A:
 4      for b in B:
 5        for c in C:
 6          if a == b == c:
 7            return False    # there is a common value
 8    return True             # sets are disjoint

This algorithm loops through each possible triple of values from the three sets to check if those values are equal.
If each set has size \\(n\\), then the worst-case time complexity is \\(O(n^3)\\).
Note that if two selected elements from \\(A\\) and \\(B\\) do not match each other,
it is useless to check the matching with the values of C; thus, the following code gives an improved version::

 1  def disjoint2(A, B, C):
 2    # Return True if there is no element in common to all three lists
 3    for a in A:
 4      for b in B:
 5        if a == b:           # check C only if there is a match from A and B
 6          for c in C:
 7            if a == c        # here a == b == c
 8              return False   # there is a common value
 9     return True             # sets are disjoint

The loop over \\(A\\) requires \\(O(n)\\) time.
The loop over \\(B\\) accounts for a total of \\(O(n^2)\\) time, since that loop is executed \\(n\\) different times.
Then, test ``a == b`` is evaluated \\(O(n^2)\\) times.
There are now \\(n\\)x\\(n\\) pairs \\((a,b)\\) to consider but, under the previous assumption that
no individual sequence contains duplicate values, there can be at most \\(n\\) pairs with a equal to b.
Therefore, the innermost loop, over \\(C\\), and the commands within the body of that loop,
use at most \\(O(n^2)\\) time. The total time spent is \\(O(n^2)\\).

Section 5.4 Element uniqueness
==============================

Given a single sequence \\(S\\) with \\(n\\) elements, we want to decide if all elements of that collection
are distinct from each other.
The first solution to this problem uses a straightforward iterative algorithm, that
checks if all distinct pairs of indices \\( (j,k) \\), with \\(j < k\\), refer to elements that are equal::

 1  def unique1(S):
 2    # Return True if there are no duplicate elements in sequence S
 3    for j in range(len(S)):
 4      for k in range(j+1, len(S)):
 5       if S[j] == S[k]:
 6          return False        # there is a duplicate pair
 7    return True               # all elements are distinct

The first iteration of the outer loop causes \\(n-1\\) iterations of the inner loop,
the second iteration of the outer loop causes \\(n-2\\) iterations of the inner loop, and so on.
Thus, the worst-case running time of this function is proportional to
\\((n-1)+(n-2)+ \\ldots +2+1\\), that is \\(O(n^2)\\).
Observe that if the sequence of elements is sorted, any duplicate elements will be placed next to each other.
Thus, all we need to do is to perform a single sweep over the sorted sequence, looking for consecutive duplicates.
A Python implementation of this algorithm is as follows::

 1  def unique2(S):
 2    # Return True if there are no duplicate elements in sequence S
 3    temp = sorted(S)            # sorting of S
 4    for j in range(1, len(temp)):
 5      if temp[j-1] == temp[j]:
 6        return False            # there is duplicate pair
 7    return True                 # all elements are distinct

The function ``sorted`` returns a copy of the original list, with elements in increasing order.
The average-case running time is \\(O(n\\log n)\\).
The subsequent loop runs in \\(O(n)\\) time, and so the entire algorithm runs in \\(O(n \\log n)\\) time.


Section 6. A mathematical approach: loop invariants
---------------------------------------------------

In the previous sections we have provided some examples of complexity evaluation of algorithms.
The approach is basically always the same: an algorithm is written (either using a
pseudo-code or a programming language), and the evaluation of basic (constant-time) operations
and nested loops occurring in the algorithms allow us to establish a relation between the size of the input and
the number of operations performed.
In other words, we are able to find an "agreement" about the time consumption of the algorithm,
but this doesn't imply that we have given a formal proof of that time bound.

One of the techniques used in the field of complexity analysis is called the *loop invariant*;
this technique is similar, if not equal, to the mathematical technique called *induction*.
Imagine that we want to prove that a statement \\(L\\) about a loop is correct;
we define \\(L\\) in terms of smaller statements \\(L_0, L_1, \\ldots , L_k\\), with:

- the initial claim, \\(L_0\\), is true before the loop begins;
- if \\(L_{j−1}\\) is true before the iteration \\(j\\), then \\(L_j\\) will be true after iteration \\(j\\);
- the final statement, \\(L_k\\), implies the statement \\(L\\) to be true.

Given the following code for the function ``find``, that should find the smallest index at
which the element ``val`` occurs in the sequence ``Seq``, we use the loop-invariant to
prove its correctness::

 1  def find(Seq, val):
 2    # Return index j such that Seq[j] == val, -1 otherwise
 3    n = len(Seq)
 4    j = 0
 5    while j < n:
 6      if Seq[j] == val:
 7        return j
 8      j += 1
 9    return −1

To show that ``find`` is correct, we define a series of statements \\(L_j\\),
one for each iteration \\(j\\) of the ``while`` loop:

**\\(L_j\\):** ``val`` is different to any of the first \\(j\\) elements of ``Seq``.

This claim is proven true with \\(j=0\\), that is, at the first iteration of the loop,
because there are no elements among the first 0 in ``Seq``.
In the \\(j\\)-th iteration, the element ``val`` is compared to ``Seq[j]``,
and \\(j\\) is returned if the two elements are equal; if the two elements are not
equal, then there is one more element not equal to ``val``, and the index \\(j\\) is increased.
Thus, the claim \\(L_j\\) is true for the new value of \\(j\\); hence, it is
true at the beginning of the next iteration.
If the ``while`` loop terminates without returning an index in ``Seq``, we have \\(j = n\\).
This implies that \\(L_n\\) is true, because there are no elements of ``Seq`` equal to ``val``.
Therefore, the algorithm correctly returns −1.
