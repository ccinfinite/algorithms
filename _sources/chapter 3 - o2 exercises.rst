

Chapter Three: Array based sequences - exercises and challenges
***************************************************************


Section 1. Reviewing the theory
-------------------------------

**Exercise 1.1**

Complete and execute the following piece of code (that can be found in Chapter Three, Section 3);
compare the results on your system to those reported in Chapter Three.
(Hint: although the speed may differ, the asymptotics should be similar).

Then, modify the code in order to demonstrate that Python’s list class occasionally shrinks
the size of its underlying array when elements are popped from a list.
(Hint: make the list large enough before you begin to remove entries).

.. activecode:: size-of-data
   :language: python
   :caption: Prints the size of a dynamic array wrt its elements

   import sys               # includes the function getsizeof

   data = [ ] 			    # empty list
   for k in range(n):
     a = len(data) 			    # a is the number of elements
     b = sys.getsizeof(data) 	# size of data in bytes
     print('Length: {0:3d}; Size in bytes: {1:4d}'.format(a, b))
     data.append(None) 		    # increase length by one

           # INSERT NEW CODE HERE


**Exercise 1.2**

The implementation of ``insert`` for the ``DynamicArray`` class has been introduced in Chapter Three, Section 4.2.
When a resize occurs, the resize operation takes time to copy all the elements from
an old array to a new one, and then the subsequent loop in the body of
``insert`` shifts many of those elements. Modify the following piece of code so that, in the case of a resize,
the elements are shifted into their final position during that operation, thereby avoiding the subsequent shifting.
(Hint: use two non-nested loops)

.. activecode:: insert-for-dynamic-array
   :language: python
   :caption: Insert a value at position k in a dynamic array

   def insert(self, k, value):

     if self._n == self._capacity: 		# not enough room, resize the array
       self._resize(2*self._capacity)
     for j in range(self._n, k, −1): 	# shift to right, rightmost first
       self._A[j] = self._A[j−1]
     self._A[k] = value
     self. n += 1




Section 2. A problem: building a Tic-Tac-Toe game
-------------------------------------------------

In this section, we will build a two-player tic-tac-toe game, which we can play in the command-line.
Check the following code; then, complete and run the ActiveCode at the end of the section.

Initially, we’ll build an empty \\(3 \\times 3\\) game board that is  numbered like the keyboard’s number pad.
A player can make their move in the game board by entering the number from the keyboard number pad.
To do this, we will use a dictionary,
a primitive data type in Python which stores data in “key: value” format.
We’ll create a dictionary of length 9, and each key will represent a block in the board;
its corresponding value will represent the move made by a player.
We’ll create a function ``printBoard()`` which we can use every time we want to print the updated board in the game::

 theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
             '4': ' ' , '5': ' ' , '6': ' ' ,
             '1': ' ' , '2': ' ' , '3': ' ' }

 def printBoard(board):
   print(board['7'] + '|' + board['8'] + '|' + board['9'])
   print('-+-+-')
   print(board['4'] + '|' + board['5'] + '|' + board['6'])
   print('-+-+-')
   print(board['1'] + '|' + board['2'] + '|' + board['3'])

Now, in the main function, we’ll first take the input from the player and check if the input is a valid move or not.
If so, we’ll fill that block; otherwise, we’ll ask the user to choose another block::

 def game():

   turn = 'X'
   count = 0

   for i in range(10):
     printBoard(theBoard)
     print("It's your turn," + turn + ".Move to which place?")

     move = input()
     if theBoard[move] == ' ':
       theBoard[move] = turn
       count += 1
     else:
       print("That place is already filled. Move to which place?")
       continue

We check now a total of 8 conditions, looking for the winning one.
Whichever player has made the last move, we’ll declare that player as a winner;
otherwise, if the board gets filled and no one wins, we’ll declare the result as a tie::

 # check if player X or O has won, for every move after 5 moves.
 if count >= 5:
   if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # across the top
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # across the middle
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # across the bottom
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # down the left side
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # down the middle
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # down the right side
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break

 # If neither X nor O wins and the board is full, declare the result as a tie.
 if count == 9:
   print("\nGame Over.\n")
   print("It's a Tie!!")

 # change the player after every move.
   if turn =='X':
     turn = 'O'
   else:
       turn = 'X'


Finally, we ask the users if they want to restart the game::

 board_keys = []
 for key in theBoard:
   board_keys.append(key)

 restart = input("Do want to play Again?(y/n)")
 if restart == "y" or restart == "Y":
   for key in board_keys:
     theBoard[key] = " "
   game()


Check and complete the following ActiveCode and run the Tic-Tac-Toe game.

.. activecode:: tic-tac-toe
   :language: python
   :caption: Play the game!

   theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
              '4': ' ' , '5': ' ' , '6': ' ' ,
              '1': ' ' , '2': ' ' , '3': ' ' }

   board_keys = []

   for key in theBoard:
     board_keys.append(key)

   def printBoard(board):
     print(board['7'] + '|' + board['8'] + '|' + board['9'])
     print('-+-+-')
     print(board['4'] + '|' + board['5'] + '|' + board['6'])
     print('-+-+-')
     print(board['1'] + '|' + board['2'] + '|' + board['3'])

   def game():
     turn = 'X'
     count = 0
     for i in range(10):
       printBoard(theBoard)
       print("It's your turn," + turn + ".Move to which place?")

       move = input()

       if theBoard[move] == ' ':
         theBoard[move] = turn
         count += 1
       else:
         print("That place is already filled.\nMove to which place?")
         continue

       if count >= 5:
         if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # across the top
           printBoard(theBoard)
           print("\nGame Over.\n")
           print(" **** " +turn + " won. ****")
           break
       elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # across the middle
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # across the bottom
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # down the left side
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # down the middle
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # down the right side
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break

       if count == 9:
         print("\nGame Over.\n")
         print("It's a Tie!!")

       if turn =='X':
         turn = 'O'
       else:
         turn = 'X'

     restart = input("Do want to play Again?(y/n)")
     if restart == "y" or restart == "Y":
       for key in board_keys:
         theBoard[key] = " "

       game()

   if __name__ == "__main__":
     game()








Section 3. Exercises and self-evaluation
----------------------------------------

**Exercise 3.1**

Let \\(A\\) be an array of size \\(n \\geq 2\\) containing integers from 1 to \\(n−1\\),
with exactly one repeated. Describe a fast algorithm for finding the integer in \\(A\\) that is repeated.
(Hint: Do not sort A)

**Exercise 3.2**

Compute the sum of all numbers in an \\(n \\times n\\) data set, represented as a list of lists,
using standard control structures.
(Hint: Use nested loops)

**Exercise 3.3**

Describe how the built-in ``sum`` function can be combined with Python’s
comprehension syntax to compute the sum of all numbers in an \\( n \\times n\\) data
set, represented as a list of lists.
(Hint: Build a list of subtotals, one for each nested list)

**Exercise 3.4**

The ``shuffle`` method of the ``random`` module, takes a Python list and rearranges it
randomly, meaning that every possible ordering is equally likely.
Build your own version of such a function, using the ``randrange(n)`` function of the ``random`` module,
which returns a random number between 0 and \\(n−1\\).

**Exercise 3.5**

Consider an implementation of a dynamic array, but instead of copying
the elements into an array of double the size (that is, from \\(N\\) to \\(2N\\) ) when
its capacity is reached, we copy the elements into an array with \\(N/4\\) additional cells,
going from capacity \\(N\\) to capacity \\(N+N/4\\).
Prove that performing a sequence of \\(n\\) append operations still runs in \\(O(n)\\) time.

**Exercise 3.6**

Given the following piece of code for the ``DynamicArray`` class,
implement and add a ``pop`` method that removes the last element of the array,
and that shrinks the capacity, \\(N\\), of the array by half any time the number of elements in the
array goes below \\(N/4\\).

.. activecode:: insert-for-dynamic-array-2
   :language: python
   :caption: Insert a value at position k in a dynamic array

   import ctypes					# provides low-level arrays

   class DynamicArray:
     def  __init__ (self):
       self._n = 0 				    # number of elements
       self._capacity = 1 			# initial capacity
       self._A = self._make_array(self._capacity) 			# define low-level array

     def __len__ (self):
       return self._n

     def __getitem__ (self, k):
       if not 0 <= k < self._n:
         raise IndexError('invalid index')
       return self._A[k]

     def append(self, obj):
       if self._n == self._capacity:			# full array
         self._resize(2*self._capacity) 		# double capacity
       self._A[self._n] = obj
       self._n += 1

     def _resize(self, c):
       B = self._make_array(c) 			  # new array
       for k in range(self._n):
         B[k] = self._A[k]
       self._A = B 						  # A is the new array
       self._capacity = c

     def _make_array(self, c):
       return (c*ctypes.py_object)( )


**Exercise 3.7**

Prove that when using a dynamic array that grows and shrinks as in the
previous exercise, the following series of \\(2n\\) operations takes \\(O(n)\\) time:
\\(n\\) append operations on an initially empty array, followed by \\(n\\) pop operations.

**Exercise 3.8**

The Python syntax ``data.remove(value)`` removes the first occurrence of element ``value`` from the list ``data``.
Give an implementation of a function ``removeall(data, value)``, that removes all occurrences
of ``value`` from the given list, such that the worst-case running time of the function is \\(O(n)\\)
on a list with \\(n\\) elements.

**Exercise 3.9**

Let \\(B\\) be an array of size \\(n \\geq 6\\) containing integers from 1 to \\(n−5\\), with exactly five repeated.
Describe an algorithm for finding the five integers in \\(B\\) that are repeated.

**Exercise 3.10**

Describe a way to use recursion to add all the numbers in an \\(n \\times n\\) data set,
represented as a list of lists.

**Exercise 3.11**

Write a Python program for a matrix class that can add and multiply twodimensional
arrays of numbers, assuming the dimensions agree appropriately.
